「所以我老是只討伐哥布林和狗頭人，才幾乎沒有成長吧⋯⋯」
「不如說，可能是因為受傷和老化的緣故，導致汝衰弱得更快了吧。」

相對的，最近打倒了利爪豹和雙足翼龍這種比我強的魔物，是升級了嗎。

難怪感覺身體很輕。

而且我的力氣好像也變大了，攜帶的旅行用的行李感覺完全不重。

「貴族有很多高等級的人也是因為這個原因嗎？」

他們擁有財力。

如果配備強大的武具來裝備的話
應該可以毫不費力地打倒比自己強的魔物、提高等級吧。

雖然偶爾冒険者中也會有像是不能繼承家業的貴族三男之類的
但是他們總能以很快的速度，提升自己在公會的排名。

或許是公會屈服於他們家的權力了吧，也可能是依靠強力武器的性能。

『也就是說，如果是現在拿著最強的劍的汝，就可以用更快的速度來提升等級！』
「確實。」
『庫庫庫，似乎理解了吾的可貴之處了嗎？，那麼再好好的稱讚吾吶！比起口頭上的話語，我更希望汝能用行動來表達感謝吶！具體來說的話，就是要更多的做──』

好的好的，真不巧現在是賢者模式。

───

為了賺取經驗值，到傍晚前一直在王都的周圍狩獵魔物。
然後完成了工作之後，當然是要喝一杯啦！

「啊～果然冰啤酒很好喝啊～」

只有王都的酒館有著豐富且各式各樣的酒。

除了紅、白葡萄酒外，也有淺色的玫瑰紅葡萄酒、

蜂蜜酒及各式各樣配料調製並混合而成的利口酒、
在北方一喝就醉、度數很高的烈火酒
還有被稱為烈性黑啤酒的深色啤酒也在吧台的櫃子上排放著。

但是，更高興的是，這裡有很多店會把冰鎮後的啤酒給排放出來。
我在原來的街道上，喝的都是常溫的啤酒，

但是對於工作了一天的疲倦的身體來說，還是冰鎮的過癮啊～

「這恰好處的苦味，且入口滑順。果然比起便宜的啤酒完全不一樣呢。」

稍微有多餘的錢，我喝了一點上等的東西。

『看汝喝的這麼開心，搞得吾輩也想來喝一杯了吶。』
「你要怎麼喝啊？」

雖說如此，但還是不要喝太多了。

⋯⋯雖然是這麼想的，不過已經醉了啊。

「嘛，好久沒回來的王都而已、OK～～OK的啦～～」

我在完全喝醉了的狀態下離開了酒館。

那是在找旅館的途中所發生的事。
即使在夜晚也非常明亮的繁華街道上，傳來了女人的聲音。

「吶，大叔，要不要和我做一些快樂的事？」

是個妓女，
年齡大概是二十三、四左右。

⋯⋯真是個美人啊。

附近形似妓院的店面有好幾家，也有其它幾個妓女在拉客。

總得來說水準相當的高啊。
該說不愧是王都嗎。

「大叔的話，可以允許你嘗試平常做不到的各種Play喔。」

她挽著我的手臂，在我的耳邊輕輕地低聲私語道。
想起了抱了瑪麗的夜晚，突然之間股間又開始發熱了。

⋯⋯現在的錢還有些富餘，應該可以吧？

而且對方是妓女的話就不用擔心有什麼後患。

『庫庫庫，果然賢者模式也不可能長時間維持呢！現在就把身體交給性慾放心地去做吧！』

感覺好像維納斯好像說了些什麼，不過沒有聽清。
之後被妓女摟著手臂進入了店內。

───

眨眼之間就到了考試當天。

『哇啊，好多人吶。這些全部都是來報考的人嗎？』
「是啊。」

集合的地點是在學院內的室外訓練場。

工作人員等也包含在內，大概有四百以上的人數。
寬廣的訓練場幾乎被人海給埋沒了。

每年的入學名額只有二十名左右。
也就是說，只有二十人能正式走進這所學院的大門。

順便說一句，在場的所有人都是平民。
貴族是和我們不一樣，是另外獨立進行的考試。

「難道說，你也來參加考試了嗎？」

突然出現的聲音，將我的思考打斷了。
是前幾天的紅髮少女。

「雖然那時我就覺得應該是這樣沒錯⋯⋯」
「你也來參加這個考試了啊，怪不得這麼厲害呢。」

年紀輕輕卻能獨自正面對抗Ｂ級上位的雙足翼龍。

就算是加爾達那樣的Ｃ級冒険者，在戰鬥之前就已經逃走了。

「我還差得遠呢，你才是真的厲害。」
「不，只有我一個人的話，是絶對不可能打倒雙足翼龍的。」
「你太謙虛了。」

少女似乎有些吃驚地說道。
然後露出了有點討厭的表情看著我，

「⋯⋯雖然是個變態。」
「啊，那不是故意的啊！」
「誰知道呢？」
『當然是故意的吶！』

你稍微閉嘴一下。

她用懷疑的眼神盯著我看，我只好摸著良心向她道歉了。

「真的非常抱歉，請原諒我吧！」
「我不介意，只是當時很想把你給斬殺了而已。」

脖子附近感覺莫名的抽痛。

「⋯⋯剛才，好像感受到了一絲殺氣？」
「沒關係，是錯覺喔。」
「在拔著劍的時候說這句話完全沒有說服力啊！」
「哎啦，對不起，不知不覺就⋯⋯但既然拔出來了，就試一下也是可以的吧？就在你的脖子周圍這邊。」
「⋯⋯那樣我會死的。」

搞不好其實非常生氣呢，她。

『那是當然的囉。女人的胸部就好比天使的翅膀一樣。如果摸了的話，就必須要好好地道謝吶！』

所以說叫你閉嘴啦！

「這麼說起來，我還沒有說自己的名字吧。我是艾莉婭。」
「我叫做盧卡斯。」
「盧卡斯、嗎⋯⋯」

少女──艾莉婭、複述著我的名字。

這麼說起來，她到底是什麼人呢？
有著氣派端正的行為舉止

所以最初我還以為她是貴族，不過沒有穿著華麗的服飾。
而且其持有的劍也不是什麼很好的東西。
用的時間非常的長了吧，刀身不僅很舊而且刀刃的損壊也很嚴重。

抱著這樣的疑問，在腦內思考的時候。

「果然來參加考試了啊，艾莉婭。」

從人潮中，一名青年向著我們這邊走了過來。