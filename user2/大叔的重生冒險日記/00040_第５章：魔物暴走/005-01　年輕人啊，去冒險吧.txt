他的名字是貝爾格利特。是王宮的下級文官。是男爵家的６男這個微妙立場。而且母親是側室，沒有繼承家業的機會。在老家大致上是被當作空氣。

母親是個聰明人，從小就教育好貝爾格利特，本人也努力學習。也許是因為這個價值吧，年紀輕輕（２０歲）就出色地在王宮工作。

懶散生活的哥哥們好像很嫉妒他。要求他能幫忙介紹工作。

據說他的父親原本沒有領地，為了資產而將大商會的女兒作為側室，他的母親的老家很富裕。

他的爺爺非常疼愛他，給了很多幫助。用那筆錢一點也不奢侈，把錢全部都投入到了學習中去。從爺爺的角度來看，他也是個驕傲的孫子。

並且不輸給微薄薪水，一邊接受爺爺的援助，一邊做著迷宮的研究。因為他的工作場所是管理迷宮的部門。

並且，注意到最近的迷宮發生的異變，感到有必要調查。一有什麼【心跳】的感覺。

在專業部門做了５年實際管理工作的人，感覺到了。那個恐怕不是杞人憂天的事，自己持有像第七感一樣的「不祥預感特化能力」，所以很明白。
（譯註：胸騒ぎ，為焦慮或不祥的預感而悸動。）

不能一笑置之。不管怎麼說，在這個迷宮裏不斷重複著無法相助的激烈戰鬥。

不要理會拘泥於保身的廢柴上司們，也不做多餘的事，讓這個部門和我們都能得到好處。

雖然是蛇足，但理所當然地這個部門也有在上一次事件中被肅清的人。反正都是直轄地區。
托你的福，他也能夠到現場控訴了，不過，他的身體還可以。

「妳怎麼想？」
問了真理。

「是啊。確實最近淨是些奇怪的事情。我以長時間來看，並不會那麼介意。不過，這的確可以說是魔物暴走的徵兆。」

原來如此。看來有調查的必要。他是工作狂，不會有那樣的空閒吧。
嗯？他在王宮工作吧。是嗎……。

「喂。明天一起去吧。」

◆◆◆◆◆◆◆

我的名字是貝爾格利特。為什麼會變成這樣呢？這裡是王宮。是我工作的地方。
但是……可是，這裡不是謁見間嗎？

這個人啊，隨便走著去，突然來到了拜見之間，對警衛士兵說道。

「Ｓ級的勇者現在想見國王，可以幫忙問一下嗎？根據情況也可能成為國家危機的內容，如果沒有傳達，你可能會被問罪。」

士兵先生，僵硬地敬禮了。

「哈哈。就是現在！」

慌慌張張地進去了。另一個士兵也用畏懼的目光看著這個人。我竟然和危險的人發生了關係。

然後，現在我們正站在國王陛下的面前。我快要暈倒了。

「從你嘴裡說出國家危機之類的騷亂話，真是無法置之不理。你給我解釋清楚。」

「是的。簡單地說，在迷宮裏可以認為有魔物暴走的徵兆。」

阿爾馮斯先生與國王陛下正在對話。感覺上他們以前也見過面。

「什、什麼！？」

「如果現在王都遭到毀滅的話，鄰國一定會乘機進攻的。這可謂是國家的危機了。」

「過去……經歷了許多危機。但是封鎖迷宮是不可能的。鄰國有很多迷宮，我國只有兩個。也有只在那裡生產的資源。還有戒指的存在。」

「聽專家講，我們的真理也覺得有那個可能性。因為她是實際親眼見證過好幾次的活證人。更加我也一直感到不協調感。畢竟是當事人中的當事人。」

「嗯。有調查的必要嗎？話雖如此，這件大事竟然被王宮的局外人拿走了。」

「啊，這傢夥一直控訴著，被上面的人捏碎了。」

「喂，宰相。是哪裡的混蛋幹的！」

那個人是宰相嗎？第一次知道呢。我不知道偉人的長相。

「那是……應該是前幾天因案件被處刑的那些人吧。他在迷宮的管理部門。非常優秀。」

被誇獎了。為什麼這麼了不起的人會知道我呢？
國王陛下一副咬碎苦蟲般的臉，不過。

阿爾馮斯含笑對國王陛下說。

「我想以這樣的理由去調查，因為他在王宮工作。請允許他離開正常工作。如果有成果的話，請讓他晉昇。上面空著吧？為了陛下的利益，你應該把這些人放在合適的位置上。」

暗地裡譴責著陛下說，你怠慢職務了。
陛下也苦笑著，硬以極好的笑容返回了。

「嗯。我們約定好了。那麼馬上去調查比較好。費用請稍後支付。不，宰相。讓他先拿準備金。然後，和那裡的文官部門聯絡，讓工作能夠順利進行。」

「那我們趕緊去吧。那麼，你。先向組織打個招呼。因為是國王陛下的敕命。我們馬上就去。」
阿爾馮斯逼著我。

啊，於是，我突然拉開了貝爾格利特大冒險的帷幕……