「這裡是？」

在布洛森小姐的帶領下，我和瑪麗一起抵達了新的城鎮。
穿過位於『起始之城』北邊的地下通道後出現在眼前的這座城市，是簡直就像是實際【現實】的義大利的威尼斯那樣的都市。

「這裡是『貝爾森斯托赫市』，暫時在這裡和『起始之城』周圍滾來滾去吧」
「『滾來滾去？』」

滾來滾去是甚麼？突然從總是態度不客氣的布洛森小姐口中聽到可愛的單詞，令我嚇了一跳⋯⋯雖然瑪麗好像並不吃驚，不過果然她也不明白那個單詞的意思吧，她和我一起提出了疑問。
移動的時候差點就摔倒了，必須小心才行。

「⋯⋯就是來回走動」

⋯⋯原來如此，滾來滾去是指轉來轉去嗎。怎麼說呢，好像跟布洛森小姐說的有些隔閡，我抱有一種很難說出來的感覺。

「在這個城市主要是要提升由於帶練升級的弊病導致停滯不前的技能等級」
「在『起始之城』不行嗎？」

確實利用PK和重視效率的狩獵使基礎等級提升相當多了，而技能等級也必須提升的這一點是可以理解的，但有必要特意移動到其他地方嗎？

「如果打倒跟自己所屬陣營敵對的人的話，技能等級就會有額外的提升哦」
「哦〜」

布洛森小姐回答自然的疑問後，瑪麗接受了⋯⋯似乎是因為在這個『貝爾森斯托赫市』裡，無論是屬於秩序陣營的人還是怪物都很多，對於在『起始之城』進行PK等等的事後業力值下降了的現在來說剛剛好嗎。

「另外這個遊戲因為哪誰的錯導致PVP變得比當初的多，所以最好還是習慣對人戰呢」
「？是這樣嗎？」
「嗯，而且因為這個城市仍然還未從革命的混亂中平復過來，我們就是瞄準著這一點哦」

換句話說，在從外面看也覺得錯綜複雜的道路裡襲擊這座城市的強大NPC和玩家就是最初的目的吧⋯⋯雖然怪物的話無所謂，但對人戰的話還是不習慣呢。
總之假裝聽不到革命之類的危險的單詞吧。

「不過，辦不到的話⋯⋯」
「⋯⋯請至少限定是能復活的玩家」

嗯，如果限定是玩家的話就沒問題吧⋯⋯雖然不知道義姊有多強，但山本先生說的時間應該是從服務開始的第一日左右就開始玩的了，那麼至少到追上為止⋯⋯⋯

「⋯⋯這就可以嗎？」
「嗯，反正從一開始這就是這樣的遊戲對吧？」

沒錯，Karma Story Online⋯⋯略稱KSO的這個，似乎從一開始就是殺氣騰騰的遊戲，對玩家同志之間的突襲、掠奪、背叛是十分提倡（秩序陣營除外）的⋯⋯雖然似乎由於某人的錯使得這些事情出現的機會增加了。
而且如果想成是劍道的實戰訓練的話，就應該要專心致志、竭盡全力為提升技能等級而努力吧⋯⋯雖然亦包含著對瑪麗的擔心。

「而且已經走進這樣的小巷⋯⋯是小巷吧？已經走進這裡了」

旁邊的水路上也有船隻在來往著，因此它有一定的寬度使得陽光能照射下來，所以這裡不是很暗⋯⋯然而就場所而言，這裡離開了大街、道路也是錯綜複雜的，所以我覺得這裡是小巷⋯⋯不過因為我不知道詳情，我對這一點沒有把握。

「因為那孩子毫無遲滯地先走一步了⋯⋯」
「⋯⋯」

哎⋯⋯真是的，為甚麼瑪麗如此毫不猶豫呢？明明甚至會因跌倒受傷時流出的血而嚇倒的，為甚麼？因為是遊戲嗎？
我一邊想著這種事一邊追上瑪麗，但她忽然停下站著不動了⋯⋯那傢伙是怎回事啊。

「喂，瑪麗？以為你先走一步的時候你卻忽然停下了，怎麼了？」
「⋯⋯」
「喂？」

突然之間那傢伙是怎樣了⋯⋯喊她也沒有反應，該不會是受到哪裡的攻擊了嗎？
既然布洛森小姐說這是有效率的話，那麼這裡有其他亦是以PK為目的的玩家在亦非不可思議的事。我應該警戒突襲的⋯⋯！

我想到這個可能性的同時，首先確認作為人類最大的死角的上空，但沒有發現任何人影。

「⋯⋯馬薩，在那裡」
「唔！敵人嗎？！」

我看向到剛剛為止都在發著呆的瑪麗緩緩地舉起手所指著的方向⋯⋯一邊將手放到腰間的刀上準備好隨時都可以拔刀一邊確認，而前方的是──

「──義姊？」

像是在與誰聊天著般自言自語地走著的義姊。
當然也有這是別人的可能性，但除了不過只是將頭髮變短並加入白色的挑染之外，其他的地方看上去全然是同一個人⋯⋯我們也只是改變了頭髮和瞳孔的顏色的，這幾乎肯定就是義姊吧。我不知道為甚麼布洛森小姐的臉上露出了非常厭惡的表情。

「義姐大人！」
「喂、喂？！」
「等等你們？！」

心急如焚的瑪麗衝了過去。看見後我反射性地不得已追了過去⋯⋯雖然布洛森小姐出聲制止但我顧不得了⋯⋯不知為何有討厭的預感。

「為甚麼偏偏是那樣的危險人物⋯⋯？！」

好奇怪⋯⋯明明我們兩人應該都是為了能見到義姊並與她一起玩、使得關係變好才開始玩這個遊戲的，但如果現在不阻止瑪麗的話，似乎就會受到無法挽回的傷害⋯⋯我止不住這種預感。
雖說是同父異母但應該是姊妹來的，但為甚麼⋯⋯我感到如此不寒而慄。

「瑪麗，等等！」

果然有這種程度的吵嚷的話那邊也是會察覺到的，總是不知道在思考著甚麼的美麗臉孔望了過來⋯⋯雖然瞳孔的眼色亦變成了紅色，但不會錯的，那就是義姊。
對，明明應該是義姊來的⋯⋯但我放在刀上的手止不住顫抖。我如要甩開這種感覺般，將另一隻手伸向瑪麗──

「義姐大──嘎噗？！」

──但是，眼前的瑪麗被肯定是義姊的人用旋踢踢到下巴，猛撞到牆上了⋯⋯

發生了甚麼、被做了甚麼，自己的理解無法追上，我保持住伸出手的姿勢凝固了⋯⋯血液聚集到臉上，甚至可以聽到像是在耳邊敲打著太鼓般的心跳聲⋯⋯明明應該是VR來的，可是好奇怪，我陷入時間彷彿停止了一樣的錯覺裡。

「主人大人，突然在做甚麼？呢？⋯⋯不，因為有令人生氣的臉孔逼近了過來」
「啊，誒⋯⋯？」

明明對手應該是將這邊當作義弟、義妹來認識的，但她簡直⋯⋯就像是看見『蟲』之類的那樣，在接下來要噴灑殺蟲劑來殺蟲的時候，一邊想著甚麼其他的事，一邊發出自然的殺意，同時她──

「⋯⋯這裡是遊戲，一點問題都沒有哦？」

──拔出短刀並將視線朝向我。


▼▼▼▼▼▼▼

作者的話：
想一個人住啊⋯⋯這樣想著並尋找居所，但房租好貴啊（絕望）
（註：於２０１９年５月３１日的活動報告裡，作者指他在那時開始一個人住了。）

順帶一提有甚麼想要的愚人節捏他嗎？