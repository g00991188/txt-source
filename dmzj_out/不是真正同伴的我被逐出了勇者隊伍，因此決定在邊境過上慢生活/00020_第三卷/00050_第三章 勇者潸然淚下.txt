妹妹就在我的懷中。
是我以為至少在打倒魔王之前，但甚至也有可能再也無法相見的妹妹。

「哥哥！」

環繞在我背後的露媞的手臂加大了力氣。
她在笑，在邊哭邊笑。

在我旁邊的岡茲，還有和露媞一起進來的那個喜歡吃竹輪的女孩都目瞪口呆地愣在了那裡。
我不知道該說什麼好⋯⋯⋯總之我也用力抱住了露媞。
能夠和妹妹重逢⋯⋯我也非常開心。打從心底。
過了一會兒，露媞似乎也冷靜了下來，我輕輕地推了一下露媞的肩膀，她也老實地鬆開了手。
表情似乎也恢復了原狀。
雖然她現在也在笑，但在不了解她的人看來就是面無表情。

「哥哥⋯⋯不是的」
「⋯⋯？什麼意思？」
「我對亞雷斯沒有任何想法」

難道，她是指在我離開的時候被亞雷斯摟住肩膀那件事？

「是嗎？我還以為⋯⋯」
「不是的」

露媞很少見的在我面前用強烈的語氣做出了否定。
這是她特有的方式，表明了徹底否定的態度，因此我也老實做出了讓步。

「我明白了，看來是我誤會了」
「對」

露媞很難過似地訂正道。這樣啊，她和亞雷斯不是那種關係啊⋯⋯⋯雖然這讓我很高興，但相對的也讓我得知了露媞最後變得無依無靠，又讓我很難受。

⋯⋯差不多也得和周圍的人解釋一下了。

但是，該怎麼解釋才好啊。

「她叫你哥哥，就是說她是雷德的妹妹嗎？」
「哥哥，難道你就是基甸先生嗎？」
「基甸？」
「雷德？」

岡茲和喜歡吃竹輪的女孩都歪了歪頭。
啊啊，我該怎麼解釋啊。

＊  ＊  ＊

莉特出去買晚飯的材料了。在她回來之前有很多事得向露媞解釋。同時也得向岡茲解釋。
雖然岡茲的嘴巴不是很牢，但他也分得清什麼事該說什麼事不該說。

「嗯──」

我臨時關了店。在這種情況下實在沒辦法營業。

「呃，首先，岡茲」
「噢」
「這孩子是我妹妹。但是這件事我並沒有告訴過其他人。之後我會好好向你解釋的，如果現在你能什麼都別問老實回去就幫大忙了」
「沒問題，雖然我不了解詳情但有一點很明確，那就是你們看上去並不討厭對方」

岡茲咧嘴笑了笑。

「妹妹可是很寶貴的」

岡茲也有娜歐這位妹妹在。
他們兄妹關係很好，娜歐的丈夫米德，還有她的孩子坦塔也都把岡茲當做家人來看待。岡茲爽快地起身，拍了拍我的肩膀。

「呃，妹妹啊。雷德⋯⋯基甸可能才是本名來著？雖然我不是很了解你們之間有什麼，但這傢伙在佐爾坦的平民區可是個可靠的好男人。從沒做過壊事。這點你就放心吧」
「這樣啊」

露媞點了點頭。
然而，我卻覺得她的表情似乎帶有一絲陰霾。

＊  ＊  ＊

留在客廳的有我，露媞，喜歡吃竹輪的女孩⋯⋯名字好像是叫提瑟，她似乎是代替我由亞雷斯推薦加入的擁有『刺客』加護的殺手。
話說我的職責和『刺客』的職責應該天差地別吧。

「啊──，該從哪裡開始說明好呢」
「哥哥」
「怎麼啦？」
「你在和別人一起住？」

露媞環顧室內如此問道。
唔，在我解釋之前就先注意到了嗎。
屋內並沒有什麼可以證明莉特存在的東西。
但是從擺放的花瓶和裡面的花，還有對餐具的品味這些方面都能看出這裡住的不止我一個人。向妹妹報告這件事讓我有些緊張。

「我在和別人一起住」
「⋯⋯這樣啊」
「她應該馬上就回來了，那什麼，我們在羅加維亞的時候不是有一位叫莉茲蕾特的彎刀使公主和我們一起行動過嗎」
「莉特啊」

露媞有些難過地這麼說道。
以為作為我的代替她粘上了亞雷斯，看來真的是我的誤會。
露媞在啟程的時候⋯⋯不，是從小時候開始眼裡就只有我一個人。

「總之，我會把為什麼會變成這樣的實情告訴你的。你應該從亞雷斯那裡聽說了我是由於偵察任務才掉隊的這件事吧」
「在那之後，雅蘭朵拉拉就開始主張是亞雷斯殺了哥哥，然後他告訴我們哥哥只是逃跑了」

亞雷斯那混蛋，竟然沒有遵守約定。雖然離隊的我可能也沒有抱怨的資格吧。
我把亞雷斯嫌我是個累贅所以讓我離開隊伍，然後我變得自暴自棄，之後流落到這個佐爾坦，開了一家藥店⋯⋯又和莉特同居的來龍去脈都告訴了她。

「我是打算繼續和莉特一起在這裡生活的。將來也會結婚」

明白地把結婚這個詞說出口讓我有點緊張。
莉特是公主，雖然我是騎士，但那也只不過是從平民升上來的單代爵位而已。
雖然我們之間門不當戶不對⋯⋯但我和莉特都已經做好了捨棄自己身世的打算。

「這樣啊」

露媞大概是從我的表情上看出來我是認真的吧。關於我們之間身份差異這點她並沒有多說什麼，只是靜靜地點了點頭。

「抱歉，我擅自走掉了」
「⋯⋯這都是亞雷斯的錯。不過」

露媞筆直地看向了我。

「亞雷斯我會讓他閉嘴的。所以，可以吧？」
「⋯⋯⋯⋯」
「莉特也可以一起來。哥哥，我們再繼續一起旅行吧」

露媞懇求般說道。這刺痛了我的心。
我覺得⋯⋯除了自己之外露媞也有其他人可以依靠。

亞雷斯，達南，提奧朵拉，雅蘭朵拉拉。

雖然大家也有缺點但都比我要強大，是一群在各自的領域登峰造極的同伴。
我認為即便我不在，亞雷斯的魔法，達南的拳法，提奧朵拉的槍術和奇跡，雅蘭朵拉拉操縱植物的力量⋯⋯他們都能夠給予露媞支援。

「哥哥不在的話，隊伍就沒辦法運作。把亞雷斯趕出去也沒關係。我們需要哥哥」

露媞現在說明起了隊伍裡的情況。
亞雷斯想要獨自承擔我的工作，結果以失敗告終。
達南外出尋找我，雅蘭朵拉拉則認為我被殺死而離開了隊伍。
後來提瑟似乎作為我的代替加入了隊伍，但脫隊３人只進隊１人人數根本不夠。

「⋯⋯」

打倒了風之四天王，我還以為勇者隊伍的旅途會一帆風順。

但是，事實上並非如此。就和莉特擔心的一樣，在我離開之後出現了嚴重的問題。如果我想回去，那麼勇者隊伍就還有我的一席之地。只要我希望，那麼就能再次回歸冒険的日常。

然而，即便如此⋯⋯即便如此我也。

「抱歉露媞。我已經，找到了留在這裡的意義」

不止是莉特，這家店，這裡的日常，都成為了我生活的意義。能滿足佐爾坦的居民們那小小的盼望⋯⋯和這些一臉開心的收下只是用於溫暖身體的懷爐的人們生活讓我很幸福。
這裡⋯⋯佐爾坦已經成為我的歸宿了。

「這樣啊」

露媞像是已經半預料到一樣，靜靜地答道。
接著，

「那我也要在這裡住下」

我最深愛的妹妹，做出了和我一樣停止冒険的決定。
任性自私的發言，要逃離賭上世界命運的戰鬥的意志。
然而，又有誰能責怪她的這番話呢。
到底該說什麼才好。我又能說得了什麼。
我的思考陷入了苦澀的漩渦之中。
然而這並沒有讓我覺得痛苦。

因為在我眼前的這位面無表情的臉上充滿了難以抑制的悲傷的少女，是我深愛的妹妹。
沒過一會兒，拎著購物袋德利特便回來了。

「我回來了了──！雷德不在嗎？怎麼關店了」

我慌忙走向店裡。

「在呢，我在客廳⋯⋯有客人來了」
「客人？」

地板咯吱地響了起來。應該是有人從我背後的門探出頭來了吧。
雖然我看不見背後⋯⋯⋯

購物袋啪地掉在了地上。
莉特被嚇的連話都說不出來，定在了那裡。

「啊──，嗯，我妹妹來了」

就算看不見，只要看莉特的表情就能很清楚在我身後的是誰。

「好久不見」

露媞小聲對莉特打了聲招呼。

＊  ＊  ＊

咣地一聲響起。

這是我把倒有咖啡的杯子放在桌上的聲音。
現在屋裡安靜到如此輕的聲音都能聽的清楚。

（好尷尬）

莉特和露媞都在看著手邊的杯子，不願看向前方。
提瑟則在盯著趴在她手背上的小蜘蛛。
蜘蛛環顧周圍，做出了像是在擔心提瑟一樣的動作。

「啊──，露媞。你們住在哪？」
「港區的旅館」
「港區嗎，比起那邊中央區或者北區的旅館條件不是更好嗎？」
「沒關係」
「這樣啊⋯⋯那接下來你打算怎麼辦？今天要不要住下來？」

露媞的表情唰的明亮了起來，但又馬上低下了頭。

「不了，港區那邊還有事沒處理完⋯⋯不過，處理完之後那天我想和哥哥待在一起」
「好啊」

有事要處理嗎⋯⋯⋯

「還沒問你呢，為什麼你們會來佐爾坦？」
「其一是在找哥哥」
「找我⋯⋯？」
「討伐魔王哥哥是必須的」

嗯──⋯⋯不管是見面的時候，還是宣布要住在這裡的時候⋯⋯以及這個是來找我的理由，怎麼想都有種違和感。

「其二是還有一個人要找」
「找人？」
「是一個隱藏在佐爾坦的知識分子。他擁有討伐魔王所需要的知識。不過我已經找到他了所以不用擔心」
「是嗎」
「那個！」

一直沉默不語的莉特開了口。

「雷德⋯⋯你要怎麼辦？」

對了，還沒告訴莉特呢。

「我會留在這裡的。還要和你一起開店呢」
「真的嗎？⋯⋯但是」

莉特瞥了一眼一臉難過的露媞。

「莉特不用在意。我⋯⋯也要在這個鎮子住下」
「誒，誒誒！？」
「今天就先回去了」
「回去⋯⋯」

露媞站了起來。不對勁，從剛才開始就感受到的那股強烈的違和感一直揮之不去。
我覺得發生在露媞身上的，決不只有壊事⋯⋯雖然是這麼覺得，但感覺那又不見得都是好事。

「哥哥」
「你隨時都可以再來。我就在店裡」
「我就是想問這個的」

露媞害羞地笑了笑，隨後低下了頭。
我溫柔地撫摸起她的頭來。

「嗯⋯⋯」
「我還沒和你聊夠呢。我離開之後的发生事，咱們應該都有很多想講的吧」
「這樣啊，但是今天就只能先到這兒了⋯⋯」

露媞筆直地看向我⋯⋯又露出了很開心的表情。

「不要緊，從今往後我也會有很多時間的」

旁邊的莉特看到露媞的那張笑容大吃一驚。

＊  ＊  ＊

露媞很乾脆地就回去了。
我和莉特夾桌而坐，陷入了沉思。

「吶雷德。真的沒關係嗎？」
「你指什麼？」
「那個⋯⋯這話由我來說可能比較奇怪⋯⋯不過露媞她需要你」
「是啊」
「那你和她一起走⋯⋯會不會更好，我在想這件事」

莉特的表情看上去很難受。

「為了世界嗎⋯⋯」

老實說，我也不是沒有動搖。
回想起露媞那張悲傷的表情，我確實產生了迷茫。

「再好好商量商量吧。我和露媞，莉特，還有提瑟４人一起」
「嗯」

這並不是能夠立馬探討出結果的問題。我們還需要時間。
可能會有人跳出來非難勇者止步不前。
但如果要論罪的話，那就是我的罪過。露媞沒有做錯任何事。
因為即便想要擔負起世界的命運，露媞也還只是一名１７歳的少女。

＊  ＊  ＊

走出藥店的露媞迅速地離開那裡之後，捂住胸口呻吟了起來。

「露，露露小姐！？」

提瑟慌忙靠了過去。
露媞從懷中拿出惡魔加護一飲而盡。

「不應該說要留在這個鎮子的」

露媞額頭上浮出冷汗小聲說道。
剛才襲向露媞的，是加護傳來的強烈衝動。
衝動本應已經遭到了惡魔加護的削弱，然而面對勇者想要放棄做『勇者』的想法，這個世上最強的加護以對於正義的慾望和仿彿心臟被捏爆一樣的痛楚做出了抗議。

「必須再繼續削弱它才行」
「露露小姐⋯⋯」

提瑟不安了起來。果然現在的勇者大人有什麼地方不對勁。
提瑟和勇者一起旅行的時間雖然不長，但她現在的狀態絶不正常。

「呀啊啊啊啊啊！！！」

就在此時，她們聽到了悲鳴。提瑟迅速擺好了架勢。

然而，搶在她之前露媞便衝了出去。
在平民區邊緣，與港區之間交界的水渠旁的馬路上。
有１名高等精靈女性被拽著頭髮倒在地上。

「喂，誰允許長耳朵在這裡做生意的，嗯？」

高等精靈歐帕拉拉精心製作的關東煮被殘忍地打翻在地，２個面紅耳赤的醉漢正掛著猙獰的笑容腳踏著它們。

「不要！」
「這裡是人類大爺的城鎮，你們這群亞人跑出來可是會有損鎮子的美觀的啊」

每個城鎮都會有這種人類至上主義者。
大部分情況下，他們也不受人類的待見，可即便如此像他們這種人不管在哪個城鎮都會有足夠的人數用於構成他們的小團體。
看著歐帕拉拉被毆打得通紅的臉頰，男人妄圖滿足自己那扭曲的愉悅之心⋯⋯⋯

然而，

「誒？」

僅僅只是一個眨眼的功夫。在下個瞬間，眼前出現了一名雙腿大敞，準備揮拳的少女。他根本來不及保護自己。

「哦咳！？」

肺裡的空氣被抽空，接著仿彿內臟被攪得稀巴爛一樣的痛楚襲向了男人。
露媞以不會取他性命的力道揍了過去。
然而她並沒有手下留情。她拿捏的力道剛好可以能讓他保持意識，並感受到生不如死的痛苦。
這一拳，肯定會成為這個男人下半輩子的心理陰影吧。
男人蹲了下去，捂住腹部留著眼淚和口水呻吟起來。

「咿，搞，搞什麼！？」

另一個男人慌忙想要逃跑，然而提瑟已經繞到了他的前面。

「滾，滾開！」

提瑟抓住了想要撞飛她的男人的手臂，隨後男人便飛到了空中。

「嗚呀！！」

按住被摔在地上的男人的手臂關節，提瑟輕輕用手指壓向了男人的側腹附近。

「嗚，嗚嘎啊啊啊啊啊啊啊啊啊啊啊啊啊！！！！！！！！！！！！」

他發出了撕心裂肺一般的慘叫。她使用了刺客的人體破壊術。
這個攻擊只會給予對方痛苦但不會讓對方受傷。

「竹輪⋯⋯真浪費」

看著被踐踏的竹輪，提瑟又加大了一些手指的力氣。

＊  ＊  ＊

在把２個施暴者轉交給聽到騷動後趕來的衛兵時，他們正完全不顧體面地蹲在地上痛哭。

「這下他們就不會再犯了」

露媞看著他們喃喃道。提瑟也點了點頭。

「感謝２位的鼎力相助」

在衛兵眼裡這些男人的野蠻行為似乎也很令他們憤怒，關於露媞和提瑟動武的事衛兵沒有多做任何過問，只是向２人道了謝，之後便帶著犯人離開了。

「呼」

提瑟感到了些許滿足。身為殺手的自己，很少會像這樣去幫助別人。雖然很少這麼做⋯⋯但感覺並不壊。
與提瑟形成對照一般，露媞則有些失落。

「謝，謝謝你們！真是幫大忙了！」

用濕毛巾抵住被毆打的臉頰，歐帕拉拉朝她們走了過來。
露媞看到她的臉，把右手放了上去。

「露露小姐！」

察覺到露媞想要幹什麼，提瑟大吃一驚地喊道。
然而露媞絲毫沒有聽從勸阻，發動了「治癒之手」

「誒！？」

歐帕拉拉發出了驚訝的聲音。臉上和身體上的疼痛瞬間消失不見，紅腫起來的臉頰也恢復的完好如初。

「我還沒辦法對有難的人見死不救」
「露露小姐⋯⋯」
「抱歉，明明不能使用技能的」
「怎，怎麼會，沒關係的⋯⋯你這麼做，肯定是正確的」

沒錯，這就是勇者。
提瑟豁然開朗。自己就在正義的旁邊這件事讓她有些引以為傲。

露媞看向了自己那只鋤強扶弱的右手。
剛剛還在折磨她的衝動，因為救下了高等精靈而被緩解了。再過一段時間惡魔加護應該也會奏效了吧。
她之所以會衝過來，就是因為認為這樣可以來緩解加護的衝動。

「這真的是正確的嗎」

露媞用誰都聽不到的聲音，向自己的加護如此發問。

＊  ＊  ＊

我叫提瑟・加蘭德。是擁有『刺客』加護的勇者大人的同伴。

現在時間是晚上。
我去給被關在倉庫的錬金術師戈德溫送飯剛剛回來。
本來我們的預定是打算連夜逃離佐爾坦的，然而因為在這個城鎮裡發現了勇者大人正在尋找的哥哥，因此改變了計劃。

勇者大人的想法，似乎偏向於留在這個鎮子。
然而我們也需要戈德溫為我們做藥。

（戈德溫在這個鎮子裡是人盡皆知了⋯⋯）

現在不僅準備錬金術師的工作室，還要選在住在佐爾坦的勇者大人能夠往返的距離，並且監視戈德溫不讓他逃走才行。

（太難了）

這就是我的感想。
如果人手再多一點的話還能再想一些辦法，可這裡只有我和勇者大人２人而已。
佐爾坦是我們初次造訪的土地，沒有能夠信賴的人物。甚至連殺手公會的支部都沒有。
硬要說的話，勇者大人的哥哥基甸先生雖然感覺應該值得信賴⋯⋯⋯

「勇者大人，果然還是太勉強了」
「這樣啊」

勇者大人靜靜地點了點頭。

「在我們弄夠藥之前還是先到別的城鎮去吧？之後再回佐爾坦來」
「我知道」
「咿！？」

勇者大人釋放的不愉快氣場令我不禁畏縮起來。
她明明只是坐在椅子上陷入沉思，卻散發著恐怖的壓迫感。
我還覺得見到基甸先生之後她身上的氛圍變得柔和了一下，結果是我想多了。

「明天，有個地方我想去調查一下。只要１周就好，在那之前希望你能找一個可以藏身的地方」
「１,１周的話我倒是還有辦法⋯⋯您有想要去調查的地方？」
「停放飛艇的地方旁邊的那座山裡似乎有古代精靈的遺跡。只要設備還能使用就可以當做藏身之處」

她是什麼時候得到這種情報的。

「木精靈好像也居住在那座山的附近，也聽說那裡簇生著可以用得上的植物」
「古代精靈的遺跡竟然和木精靈的遺跡同時存在嗎」

真少見。還是第一次聽說古代精靈的遺跡會和木精靈生活的地域在同一個地方。
不過，這也可能只是因為現在的我們並不知道這件事而已。

木精靈似乎認為大自然是會自我循環的。
他們的建築物全都與大自然融為一體，當木精靈不在了之後隨著樹木的生長木精靈的遺跡也會完全消失不見。在殺手公會教授我歷史的教官是這麼告訴我的。
其實在其他的古代精靈遺跡上面也存在著木精靈的遺跡，但它們只是被隱藏了起來，而我們並沒有注意到⋯⋯說不定就是這樣。

「如果古代精靈的遺跡還能使用，的確可以作為藏身之處⋯⋯但是，為什麼」

為什麼不惜做到這一步，也要留在佐爾坦。
然而看到勇者大人那認真的表情之後，使得我把後面的話噎了回去。

好可怕⋯⋯⋯

一只十分迷你的小腳拍了拍我的肩膀。
嚇嚇先生衝我歪歪頭。
怎麼了？它在說些什麼。
不要想太多？不，是不要想得太複雜？還有要我看清楚？
嚇嚇先生很少見的主動想要和我交流，並且還這麼囉嗦。
現在它也仍在頻繁地動著２條前足，想要向我傳達自己的想法。

「怎麼了？」

我有些不安。明明嚇嚇先生想要像我傳達些什麼，我卻不明白它的意思。
為了能夠搞清楚它想表達什麼，我也會反問回去，然而嚇嚇先生只是在不斷重複同樣的影像。這是怎麼回事？好久沒遇到過這種情況了。

⋯⋯因此我的注意力全都放在了嚇嚇先生身上，疏忽了勇者大人。

「提瑟」
「誒？」

回過神來，勇者大人已經來到了我的眼前。
然而，勇者大人的視線並不在因為吃驚而僵住的我的身上。
她的視線朝向的方向是我的肩膀。目標是在我的肩膀上歪著腦袋的嚇嚇先生。
勇者大人朝我的肩膀伸出了手。

我的思考停止了。恐懼與混貫穿了我的後背。
可能是我做了什麼惹怒了勇者大人。

但是！唯獨嚇嚇先生！！
回過神來，我已經跳向後方，拔出劍架了起來。
牙齒在咯咯作響。朝著絶對無法戰勝的對手拔劍的這股恐懼，使得我的大腦仿彿像是在被灼燒一樣熾熱。

勇者大人保持著伸手的姿勢，面無表情地停止了動作。
她筆直地凝視起我來。
雖然應該只是簡短的一瞬而已⋯⋯但我卻感覺是一段十分漫長的時間。

「⋯⋯你誤會了」

勇者大人看著我說道。

「我知道這孩子是你的寵物。並不是沒有察覺到這點想要拍死它」

她在說什麼呢？
我氣喘吁吁地在聽著她講話。然而卻完全聽不懂她在講什麼。

「我看到過它在你的旁邊手舞足蹈。也看到過你把捉到的虫子喂給它吃⋯⋯」

勇者大人一直在訴說著這樣的事。而我則依舊架著劍，渾身顫抖不止。此時一個小小的身影跳了出去。

「嚇嚇先生！？」

嚇嚇先生落在地面上之後，舉起雙腳，拚命伸展著身體擋在了我的面前。

「你，你在幹什麼⋯⋯誒？『好好看清楚』？」

到底讓我看什麼⋯⋯⋯

嚇嚇先生拚命舞動著身體，不斷重複著「好好看清楚」

接著⋯⋯我才終於「看清楚了」

「你誤會了，我並沒有那個意思」

在我眼前的是誰？是勇者大人。擁有人類最強的加護，背負著拯救世界的命運，為了正義而生，以及被所有同伴所害怕的人。

但是，我所看到的⋯⋯卻是一名惹怒了好朋友，卻不知道為什麼對方會發火而一籌莫展的少女。
不對勁。明明我因為恐懼而拔劍，進入了戰鬥態勢，勇者大人卻只是覺得不知做了什麼惹怒了我從而一頭霧水。

勇者大人實在是太強了，強到我們根本遙不可及⋯⋯因此她無法理解普通人的殺意和敵意到底是什麼樣的東西。
這可能就和明明年幼的孩子是真的發火，但大人們看了卻只是欣慰地一笑了之有些相似。

這份差異感，就是使得勇者大人一直孤身一人的罪魁禍首，我終於明白「好好看清楚」勇者大人是什麼意思了。
對了，現在我也終於看懂了過去的記憶。

在飛艇上協商事情的時候，就好比看著嚇嚇先生我會露出笑容一樣，勇者大人在看著我的時候之所以會偶爾變換表情，也是因為勇者大人看到嚇嚇先生露出了笑容。
那天晚上她像是在找什麼東西一樣，也是因為她自己也想要一只像嚇嚇先生這樣小巧的寵物。僅此而已。

終於，勇者大人似乎已經不知道該說些什麼是好⋯⋯⋯

「抱歉，我不知道為什麼會惹你生氣。但是希望你能原諒我⋯⋯對不起」

她只是一味地在道歉。咣啷一聲響起，我的劍掉在了地上。

我捫心自問，為什麼沒有注意到這些事。
並且萌生了自責的念頭。
我蹲下來，讓嚇嚇先生跳到了手背上。

（去道歉吧？）

嚇嚇先生這麼告訴我。
嗯，說得沒錯。我走向勇者⋯⋯露媞大人。
露媞大人一個激靈⋯⋯肩膀微微顫抖了一下。
我為了傳達話語，吸了一口氣。

「要道歉的，是我才對。誤會的是我。真的很對不起」
「這樣啊⋯⋯你沒有生氣嗎？」
「我並沒有生氣。露媞大人生氣了嗎？」
「沒有」
「太好了。但是，那個⋯⋯想要摸我寵物的時候，希望您能知會一聲」
「我知道了」

我把載著嚇嚇先生的手背朝露媞大人伸了過去。
露媞大人伸出左手。

嗖。

嚇嚇先生輕盈地從我的手跳到了露媞大人的手上。
接著它揚起右臂向露媞大人打了個招呼。

「⋯⋯名字」
「它叫嚇嚇先生」
「嚇嚇？」
「嚇嚇先生，這是它的全名」

露媞大人楞了一下，然後看向了嚇嚇先生。

「嚇嚇先生，我是露媞。請多指教」

露媞大人眯起眼睛露出了溫柔的微笑。

我叫提瑟・加蘭德。
擁有『刺客』的加護，現在是勇者露媞大人的朋友。

＊  ＊  ＊

古代精靈。

那是在神話時代和史冊所記載的時代之間的時代中，於世界的黎明期支配了地面的最初的種族。
首先出現的是妖精和精靈居住的第一世界。

第一世界四季都是春天，是一個可以讓不老不死的妖精們載歌載舞，永遠享樂的世界。
然而他們之間沒有爭鬥也沒有痛苦。因此他們每天都度過著無比幸福的日子，絲毫沒有打算改變。
正因為是樂園，所以第一世界是一個永遠停滯不前的世界。在無盡的歳月裡，一直觀察著第一世界的至高神迪米斯，對他們的狀況感到了不滿。

於是他便創造了第二世界，也就是這個世界。

第１天他創造了宇宙。
第２天他創造了天地日月以及繁星。
第３天他創造了作為食物的昆虫和動物以及植物。
第４天他創造了遍地的怪物。
第５天他創造了擁有智慧的精靈和龍以及惡魔。
第６天他創造了和第一世界最為優秀的種族，妖精相似的古代精靈，以及和自己相似的人類作為支配者。
第７天是他結束所有工作後的休息時間，而在那天晚上誕生了阿修羅。
第８天阿修羅為了打招呼而出現在神的面前，卻被他大罵「我可沒有創造你這樣的存在」

根據聖方教會的書籍記載，世界的誕生就是這麼個流程。
居住在暗黑大陸上面的矮人和獸人也是精靈種。而在兩個大陸上面都有繁殖的哥布林也是來自暗黑大陸的精靈種的後代。雖然也有高等精靈學者稱精靈和哥布林是不同的物種，但那並不是主流說法。
古代精靈只是方便於現代的稱呼，在現存的最古老的書籍上，古代精靈只是被記載為精靈而已，而在第５天被創造出來的精靈則被記載為「妖精」

關於現在存在的普通妖精和精靈種之間的關係，學者們經常會展開激烈的爭論，認為精靈種是妖精的一種，並且和大妖精不同，還另外存在著擁有能力可以構築起高度文明的高等妖精。不過這些只不過是主流派系的學說。在這個生物學和神學混雜在一起的世界，想要尋找真相是一件很難的事。

總之整理一下現在存在的精靈，

◯古代精靈（滅絶）→山野精靈
◯妖精＝精靈→木精靈（滅絶）→半精靈
→高等精靈
→暗黑大陸的精靈原種→矮人，獸人，哥布林

上述的系譜便是精靈學的主流派。
回到古代精靈的問題上來，他們擁有遠比現在要優異得多的文明這點是毋庸置疑的，但仍有許多的謎團。

像是能夠暫時提升加護等級的精靈硬幣，以及與之相反可以降低等級的山野精靈秘藥等等，人們認為古代精靈對加護已經做到了某種程度的解析。

在聖職者中，也有人認為就是因為他們的這種傲慢惹怒了神才導致了滅絶。
結果，他們滅亡的理由依舊不明。
只是留下了在這個時代誕生了初代魔王和初代勇者，然後初代魔王被初代勇者消滅了的傳說。

也就是說，最初的勇者並不是人類而是古代精靈。
既然如此，那這個時代的勇者也讓精靈去當就好了。

如此考慮的露媞，用降魔聖劍將在站在她的眼前發出令人不快的金屬摩擦聲的齒輪巨人給一刀兩斷。

＊  ＊  ＊

雷德採集藥草的山中沉睡著古代精靈的遺跡。露媞她們的調查之後，發現遺跡很幸運的仍在運作。

雖然上層遭到奇美拉還有盯上了古代精靈財寶而潛入的佐爾坦的冒険者洗劫，但通往下層的升降裝置還平安無事。
由於提供能量的魔力水晶力量暫時枯竭導致設備處於停運狀態，不過又因為水晶在漫長的歳月裡通過吸收周圍的魔力得以重新裝填，因此重新啟動並無大礙。
至今已經攻略過無數古代精靈遺跡的露媞運用嫻熟的手法操縱著裝置，前往了下層。
下層裡部署著在古代精靈的遺跡裡很常見的齒輪獸，而現在她們正在尋找控制齒輪獸的母體齒輪。

「呼⋯⋯」

提瑟用手背擦去了額頭上的汗。
與一臉清爽地在戰鬥的露媞不同，提瑟已經數度遭遇危機，臉上浮出了疲憊之色。

（怎麼全是護衛用的齒輪騎士啊。而且已經出現了４台在其他遺跡裡頂多只會在母體齒輪面前出現１台的齒輪巨人。甚至還能遇上用作侵略兵器的齒輪毀滅者以及潛水兵器齒輪利維坦⋯⋯這座遺跡到底什麼情況啊）

提瑟在心裡罵道。

可即便如此，由於若敵人只是單機出現的話露媞１個人便能解決，所以提瑟才能得以在這個威脅程度異常的遺跡裡順利前進。

終於２人到達了位於遺跡最深處的母體齒輪的房間。
只要破壊這裡，所有的齒輪獸都會停止活動。光是把它們的零件賣掉就能有１０萬佩里爾以上的收入，所以攻略古代精靈的遺跡對於冒険者來說正可謂是一夜暴富的美夢。

（看到它們，還有冒険者會有開心自己能一夜暴富的閑情逸致嗎）

提瑟不禁後退半步，一邊自嘲一邊如此想到。

控制所有齒輪的齒輪集合體，母體齒輪。而為了保護母體齒輪擋在它前面的，則是閃爍著光芒的金屬集合體，齒輪巨龍。

與每動一下就會發出令人不快的噪音的其他齒輪獸不同，那具擁有精妙絶倫的構造，甚至可以稱得上是藝術品的身體，即便走動也沒有發出一絲聲響。它的體內充滿了加熱過的焦油，點火用的火種在它張開的嘴巴裡仿彿火紅的舌頭一般若隱若現。

這是一種傳說前代魔王曾修復過它的相同型號，用它殺死了前代勇者的同伴並一度擊敗過前代勇者的究極兵器。
沒想到竟然真的存在，這讓提瑟浮出了冷汗。

「露媞大人！」
「我們先暫且撤退，讓基甸先生和莉特小姐也來幫忙吧」，提瑟想要如此提議。對於她們２人來說這是一個太過強大的對手。

「不要緊」

然而露媞依舊是一臉清爽的表情，將聖劍擺向右下方，拎著便朝古代精靈製造的人造龍走了過去。

＊  ＊  ＊

佐爾坦中央區的某處宅邸。

這棟建築物是某位受到其他城鎮的魔術師公會的挖角，現在已經離開佐爾坦的『召喚術士』的宅邸，它的地下室擁有魔法防御的機能，在佐爾坦十分稀有。
然而這座宅子由於陰森恐怖根本沒人肯租，使得租金比其他的宅子還要便宜，因此即便這個名為佐爾坦的国度被人指責說魔法發展實在是太落後也是無可厚非的。

「不過對我來說正好」

坐在椅子上的淺黑皮膚青年畢伊將看完的報告書丟在桌子上如此喃喃道。
佐爾坦附近的木精靈遺跡。
它們大部分都已經伴隨著草木的交迭更替而消失不見，可即便如此畢伊仍拜託了冒険者和幾個研究者們去調查它們所僅剩的那些蛛絲馬跡。
而在收集情報的同時，畢伊也在尋找自己奉命尋找的被木精靈所藏匿起來的「某個東西」

既然是被藏了起來，說明那個遺跡其他遺跡不同，現在應該仍然存在。為了查出那個地方所在，他一直在調查。
接著，現在調查已經進展到了終盤。

「這裡並沒有現存的木精靈時代的設施」

畢伊用手指敲著桌子。因為事情變得麻煩起來讓他嘆了口氣。
想要保管好物品，就需要擁有圍牆的設施。那些木精靈應該並沒有用他們自己的設施，而是把「某個東西」封印在了其他地方吧。

「難道又是老樣子藏在了大自然的險要之地裡？如果是這樣的話那就是南洋的海底或者是世界盡頭之壁了啊」

然而，這兩個地方都不是人類和惡魔能夠輕易進入的，海底那邊有水栖生物，而世界盡頭那邊則有巨龍或者古革巨人，絶對會遇到這種與環境相關的種族。
畢伊所尋找的東西，對木精靈來說是絶對不能被其他人搶走的存在，所以他們不可能會把東西封印在那種棲息著有可能會突破封印的種族的地方，哪怕機率再低。

這樣一來，那種任何人都無法入侵的地方，在這個佐爾坦就只有１處了。

「古代精靈的遺跡」

被沒有加護，卻能在超過數千年的歳月裡一直活動的齒輪戰士們所保護的遺跡。從本應會避開古代精靈遺跡所在地的木精靈，竟然會在佐爾坦裡選擇同一座大山搭建村落來看，應該是不會有錯了。

畢伊站起身，確認過通往１樓的樓梯那裡沒有人在之後將門鎖上，操作起架子上的機關。
一部分牆壁靜靜地移開，出現了通往地下的樓梯。
畢伊走下樓梯，來到了一間四周都是石壁的小房間。

這間施加了重重魔法防御的暗室，應該是前任主人的秘密研究室吧。畢伊打開了位於房間深處上著魔法鎖的架子。裡面放著１顆散發著灰色光芒的碩大寶石。
那道異常耀眼的光輝，光是看上去就會讓人害怕。然而畢伊卻若無其事地碰向寶石，沐浴著灰色的光輝。

這顆寶石是叫做「夢魔的心臟石」的稀有魔法道具。

只要將與它同步之後的灰色縞瑪瑙埋入地面，就能發動會剝奪周圍人的精靈神力的詛咒。然後積存下來的精神力會轉化為魔力，提供給寶石的持有者。
存放於前代魔王的寶庫的這個道具，對於因為沒有加護所以使用魔法的效率十分低下的阿修羅來說十分有用。

「什麼？積攢的魔力就這麼點？」

畢伊把手放上去之後寶石上面的灰色光輝立刻消散不見，變成１顆了色調黯淡的簡陋石頭。是設定出問題了嗎，畢伊集中意識進行確認，然而他並沒有發現什麼問題。

「該不會，他們已經發現詛咒制定出對策了吧」

這個詛咒應該就連妖精都無法抵御才是，他如此喃喃道。然而不管再怎麼懷疑，「夢魔的心臟石」中所儲存的魔力也還遠不及目標量。

「只靠這麼點魔力去調查古代精靈的遺跡讓人心裡很沒底啊」

得需要協力者才行。
但是，這裡是遠離魔王軍前線的邊境，不可能指望得上魔王軍的支援，佐爾坦的冒険又當不成戰力。

「這該怎麼辦」

畢伊用手指抵著太陽穴思考了起來。
殊不知露媞她們已經攻略了遺跡。

＊  ＊  ＊

自露媞來過我的店裡之後已經過了２星期。

重逢之後露媞似乎離開了佐爾坦３天，但之後她基本都在佐爾坦。她和提瑟２個人登記為了冒険者，雖然並不積極不過偶爾會接受到鎮子附近消滅哥布林的委託。
以她們的實力來說消滅哥布林這種工作實在是大材小用⋯⋯不過她們的目的大概只是緩解加護的衝動吧。

『勇者』加護的助人衝動，以及『刺客』加護的殺人衝動，討伐襲擊村落的人形生物哥布林剛好適合用來緩解這２種衝動。

雖然對手只不過是哥布林，但她們那不管數量有多少都會毫不忌諱地接下委託，簡直就像是去散步一樣輕鬆的闖入哥布林的住處，將其搗毀之後再颯爽歸來的模樣，似乎被當作十分可靠的新人掀起了一陣話題。
接著⋯⋯

───

「歡迎光臨」
「咿！？」

不知道為什麼，她今天來店裡給我幫忙了。
我試著讓她負責前台，但果然還是會散發出強者的氣場嗎，她看到進店的客人迎客之後，對方似乎就會發射性地發出悲鳴。
這深深傷害到了她本人。不過這也讓我有了新發現。

「如果笑地再燦爛些，客人大概就不會發出慘叫了吧？」
「是嗎？」

我以為注意到露媞受傷的就只有我，不過提瑟好像也察覺到了。她還會像這樣提出建議。

「這個嘛，如果你能再笑一笑肯定就沒問題了。前台可以再多拜託你照看一會兒嗎？」
「沒問題」

露媞輕輕握緊拳頭做出了加油的動作。

＊  ＊  ＊

趁著露媞和提瑟照看店裡的時候，我讓莉特去清點倉庫裡的藥物種類和數量，列了一個清單出來。
雖然平時也一直有在管理庫存，不過我和莉特商量了一下，還是趁著這個好機會仔細調查一下都有什麼比較好。

「辛苦你了」

我拿著２個倒了咖啡的杯子來到了倉庫。
莉特正拿著筆和紙，面對大量的藥物數量似乎陷入了苦戰惡斗⋯⋯⋯

「啊──！討厭！你害我忘了數到哪了！」

她發出了絶望的聲音。

「抱歉抱歉，之後我也來幫忙，先休息一如何？」
「嗯，剛好我也有點累了」

我們來到客廳在椅子上坐了下來。
能聽到從店裡傳來的露媞和提瑟迎客的聲音。

「你不用去那邊看看嗎？」
「如果我在的話讓她們工作就沒意義了吧？露媞這孩子很拘泥這方面的」
「你還真清楚呢」
「畢竟是我妹妹啊」

我可莉特同時喝了一口咖啡。

「嗯，今天味道很濃呢，還加了很多糖和牛奶。不過很好喝」

今天泡的咖啡我稍微下了點功夫。
我用了３個網眼很細的金屬咖啡濾網，把磨粗的咖啡放進去之後再倒的熱水。
咖啡堵住了濾網的網眼，所以慢慢地花了好久才把咖啡萃取出來。
由於咖啡的味道非常重，我又加入了牛奶和白砂糖，因此使得味道很濃厚。

「用香草茶來清清口吧」
「這是一種重點在於入口當下的瞬間而非餘味的咖啡呢」
「沒錯」
「謝謝你，很好喝哦」

我所採用的是可以慢慢享用味道濃厚的咖啡的沖泡方式。
旁邊還放著香草茶，適度的漱口之後，又能重新帶著新鮮的感覺去品嘗那第一口的甜美。
我們悠閑地享受著這段時間。

「多謝款待」
「只是粗茶淡飯」

莉特一臉滿足地放下了杯子。
我們稍微對視了一會兒陷入了沉默。
但是莉特很快便站了起來。

「那我回店裡去了」
「店裡？」
「差不多也該讓露媞她們休息一下了」
「那我去就行了」
「這可不行」

莉特露齒一笑。

「露媞肯定也想要享受和你一起共飲的瞬間呢」

直接把我的反駁給將死，莉特走出了房間。
我用指頭彈了一下咖啡杯。聲音很清脆。
莉特選擇的餐具雖然控制了價格，但都是良品。

「那麼，去準備她們２個的份吧」

我把咖啡放在木盤子上，走向了廚房。

＊  ＊  ＊

「辛苦你們了」

桌子上為每人都準備了３塊餅乾，還有３杯味道調的很甜的可可。

「謝謝」
「我開動了」

露媞拿起杯子喝了一口，眼睛頓時一亮。
提瑟則打算先從餅乾吃起。

「這就是冒険者用來做乾糧的東西吧？」

提瑟一臉驚訝的樣子。

「這個⋯⋯好好吃」
「裡面我加了在山上採來的樹果。味道應該比較接近肉桂吧」
「肉桂⋯⋯我沒吃過呢」
「是嗎？那晚上就做肉桂派吧」

對了。

「還有，給，這是浸過糖水的布」
「誒？」
「把這當做是那只蜘蛛的點心就行了」

言畢，我把載有一塊切下來的小碎布的碟子遞了出去。
提瑟肩膀上的蜘蛛唰地一下跳到了上面。
蜘蛛很有禮貌的舉起手和我打了個招呼，然後喝起了糖水。

「非常感謝。您注意到了啊」
「你說它嗎？畢竟你們看上去那麼親密啊」
「它叫嚇嚇先生」
「嚇嚇？」
「到先生為止都是名字」

見到我的反應，提瑟的嘴角有些開心地浮現出笑容。
雖然有些面癱，但這個叫提瑟的孩子和露媞一樣在內心裡也是個普通的女孩吧。

「哥哥」
「嗯，怎麼啦？」
「午飯可以一起吃飯嗎？」

我摸了摸露媞的頭。剛剛還在和提瑟聊晚飯的事，這孩子真是的。

「那是當然。一開始我就是這個打算」
「這樣啊」
「不只是午飯晚飯也要一起吃飯啊？」
「嗯」

露媞露出了微笑。

「我其實，最喜歡哥哥的料理了」

那是非常自然的微笑。耀眼奪目，一看就能知道那是發自內心的笑容，無比美妙。

「啊，我知道」
「這樣啊！」
「你有什麼想點的嗎？」
「⋯⋯我想喝蜂蜜牛奶」
「了解」

她點的東西並不是我想問的午飯菜單。
不過這個主意也不錯。離吃午飯的時間還有１個半小時。
就做點比較配蜂蜜牛奶的美味料理吧。

＊  ＊  ＊

到了中午我們４人圍桌而坐。

桌上擺著培根三明治和土豆泥做的奶汁烤菜，以及用在集市上的肉舖裡買來的一種體型有棕熊那麼大，名為龍雞的巨型雞的雞胸肉和洋蔥製作，使用檸檬沙拉醬調味的沙拉。
然後，就是露媞過去很喜歡喝的加了蜂蜜的熱牛奶。

「我開動了」

露媞的第一個目標果然還是蜂蜜牛奶。
她喝了一口之後雙目放光，接著一口氣喝了差不多一半。
這個喝法和露媞小時候一模一樣，讓我感到懷念不禁浮出笑容。

「啊，這個是龍雞的肉吧？真少見」

莉特吃著雞肉沙拉如是說道。

龍雞的肉在味道上雖然存在一些差異，但基本上還是雞肉，竟然能這麼快就吃出來，不愧是莉特。
看到她露出笑容，應該是合她的口味。我也感到有些開心。

「牧場裡似乎有一只擁有『野獸』的龍雞在審查時沒被發現，它大鬧一場之後逃了出去，然後就被冒険者討伐了。因此肉舖額外多出來了１整頭龍雞，所以就打折出售了」

動物的加護和人類與精靈不同種類很少。

雖然有的動物也像人類一樣，擁有『闘士』，『妖術師』，『盜賊』之類的加護，不過比例只占全體的５％。剩下的９５％加護都是『家畜』或者『野獸』
『家畜』加護的協調性很高性格傾向比較溫厚，而『野獸』加護則喜歡獨自行動並且具有攻擊性。
適合做家畜的自然是擁有『家畜』加護的動物了。牛，豬，馬，雞，山羊這些動物適合做家畜的原因也是因為它們之中的『家畜』加護出生率比『野獸』要壓倒性的大。

要做寵物的話，即便加護是『野獸』或許也能慢慢地進行調教，但商用的家畜就不能這麼做了。
一般來說牧場主會在家畜還是幼崽的時候便將它們分辨出是『家畜』還是『野獸』，然後早早地就把『野獸』給處理掉。

這種做法雖然讓我抱有一些疑問，但我對於家畜的了解程度還不到能夠對此說三道四的地步。
就像這次只是破壊了圍欄還好，然也有可能會傷害到其他家畜或者人，所以還是只能趁它們還是幼崽的時候處理掉了吧。

聽說這些雞肉來自被處理掉的『野獸』，露媞一臉認真地看向龍雞肉，然後吃了下去。
我知道露媞不愛說話，不過提瑟話也不多。
需要說話的時候她會說，也會表達自己的想法，但比如料理很好吃的話她是會說好吃，可之後就是一言不發地在默默進食。
從眼睛的動作來看她有在仔細注意我們的談話和態度，但她看來並不是那種會說客套話或者暖場話的人。

她大概就是那種只有在擁有明確話題的時候才會開口，認為語言這種東西只不過是在不得不表達自己的想法時才會用上的道具的類型吧。
因此主要在聊天的自然就變成了我和莉特。
現在我們正在告訴她們我們在佐爾坦過著什麼樣的生活。

與作為騎士每天都在東奔西走的那段日子，還有作為勇者的同伴轉戰各個城鎮與魔王軍戰鬥，解決城鎮裡發生的問題的時候相比，每天都十分平穩，露媞聽得津津有味。

「我和莉特的１天大致上就是這麼個流程」
「偶爾也會有雷德出去採集藥草不在家的情況就是了」

莉特依舊在叫我雷德。
之前晚上她曾找我商量過在露媞面前是該叫我雷德還是基甸，不過在佐爾坦我是以雷德的身份生活的，所以我們便依舊用雷德和莉特互相稱呼。

「哥哥」
「什麼事？」
「你採集藥草的地方，是西北方向的那座山嗎？」
「是啊」
「⋯⋯那個地方我知道，我去採集藥草吧」
「這感情好啊，不過可以嗎？」
「嗯沒關係」
「好吧。只用在很忙的時候就好，到時你就來幫忙吧」

露媞點了點頭。

＊  ＊  ＊

我做了一場夢。

在冬季的森林裡，林立著無數淒涼的枯樹。天空被灰色的雲朵籠罩，寒冷刺骨的風吹來，我用手捂住被凍得生疼的耳朵來取暖。
此時我只有７歳，總是會１個人跑到村子附近的森林裡來。
以小孩子的腳程森林的入口基本上５分鐘就能走到。豎起耳朵就能聽到村子裡傳來的日常生活的聲音。

直到日落之前我都一直待在那裡，等待今天一天過去。
如果待在有人的地方，指不定什麼時候就會有人需要幫助。而一旦被人求助，『勇者』便無法拒絶。
和父母關係不好的我，也不方便躲在家裡。母親在家裡織布，在她工作時只要我一靠近她就會非常不開心。

現在的我，是孤身一人⋯⋯⋯

「露媞」

我驚訝地回頭看去。那是我朝思暮想翹首以盼之人的聲音。

「哥哥」

我撲進了張開雙臂對我展露笑容的哥哥的懷中。
平時一動不動的嘴角，在哥哥的懷中也自然上揚了起來。
看到我露出笑容，哥哥也開心地笑了起來。能和哥哥一起歡笑，讓我非常開心，非常幸福。

「昨天我升格為了從士，為了讓我向家裡人報喜還有做準備拿到了１周的休假」

哥哥被巴哈姆特騎士團邀請，去了王都。還以為有一陣子會見不到他，竟然只用了半年時間便從侍童升為了從士，還從遙遠的王都趕了回來。

「至今因為一直都得緊跟著前輩騎士才行所以根本沒有休假，不過今後就有休息時間了，等有時間了我就會回來看你的」
「真的？」
「嗯，真的」

這讓我越發開心，緊緊摟住了哥哥。
接著哥哥也用力抱住了我。
雖然我想就這麼一直抱下去，但哥哥還是輕輕地放開了我。

「那就回去吧。我還沒給村裡的大家打招呼呢」
「這樣啊」

我有些遺憾。我們手牽手朝著森林的出口走去。牽著我的哥哥的手，變得比半年前更加孔武有力。

無法睡眠的我是不會做夢的。因此，這是不過回憶起了過去的記憶而已。
當時哥哥手上傳來的溫度，我至今仍然記憶猶新。
我靜靜地像黑夜伸出了手。直到２周之前，我都還以為這雙手再也觸摸不到任何東西。

但是，現在⋯⋯⋯

一邊度過著漫漫黑夜，我注意到自己正在期待著明天，獨自微笑起來。

＊  ＊  ＊

第二天。

「總之這個鎮子是沒什麼風景名勝啦」
「這樣啊」

我和露媞正一起在佐爾坦的平民區裡散步。
今天打算帶還不熟悉這裡的露媞逛一逛平民區。
天空只有稀疏的雲彩在飄動，是一個舒適的好天氣。
走在用土踩實的路上，幾個拿著樹枝玩冒険者扮演遊戲的孩子從我們身旁跑過。

「給我站住哥布林！」
「嘎噢！」

扮演哥布林的孩子可能是想要威懾，但不知道為什麼卻一邊模仿龍吼一邊跑。孩子們發出了歡樂的笑聲。
這是在平民區很稀疏平常的光景。其中１名在跑的男孩注意到我轉過頭來。

「啊，是雷德哥哥！下次也教我怎麼釣魚唄！」
「可以啊，不過現在我很忙等過一陣子吧」
「太好了！就這麼說定了！」

男孩舉起雙手發出歡呼。接著，他似乎突然注意到了什麼，露出了以他來說最為認真的表情。

「雷德哥哥」
「怎麼了？」
「你可不能花心啊！」
「她是我妹妹啦。之前才剛剛來到佐爾坦」

我苦笑著揉了揉他的腦袋。男孩一邊笑著說「別揉了」一邊握著我的手臂不樂意地搖了搖頭。

「喂──」
「啊，叫我了。回頭見雷德哥哥──！」

男孩朝著他的朋友跑了過去。
我們帶著平和的心情目送他們的背影遠去。這便是和平的佐爾坦早上的風景。
當男孩已經走遠了之後，露媞稍微歪了歪頭。

「告訴他我是你妹妹沒關係嗎？和第一次見面時那位成年半精靈不同，那孩子肯定會把我的事說出去的吧」
「你是『勇者』的事，以及我是騎士基甸的事肯定是要保密的。但是，你可是我引以為傲的妹妹。這又不是什麼壊事，所以沒必要再藏著掖著」
「⋯⋯這樣啊」

聽到我的話，露媞先是瞪大了眼睛，接著嘴角上揚輕輕點點頭。
我們走起來之後，看到了一個很熟悉的建築。

「那邊是我經常去的桑拿店。店主是一位叫澤弗的老爺爺，他是個具有匠人氣質，很有意思的人」

香袋現在也會定期給他送貨。本來我還擔心大家會不會覺得膩味，不過散發著香草香氣的桑拿似乎相當受佐爾坦居民的歡迎。現在是上午１０點左右，店裡的人已經不少了。

「桑拿」

露媞盯著澤弗的店看了起來。

「要不要去看看？」

雖然這可能會花不少時間，不過也不是說非要今天就帶她把鎮子逛完。
我離開露媞的隊伍已經有將近２年了。這繞路去趟桑拿房，還不足以填補我們之間空缺的時間。
聽到我的提議，露媞轉向了我。

「我還沒有去過這種桑拿店」
「是嗎」

在老家的村子裡，用來燻製保存食品的小屋在閑置的時候也會用作公共桑拿房，不過畢竟只是個小村子，並不是這種商業性質的桑拿店。在露媞作為勇者開始旅行之後，我們也沒有時間在遭到魔王軍侵略的地方洗桑拿，在取回和平之後為了進行善後處理我們會在當地停留，在那時都是在領主的宅邸或者皇宮裡洗桑拿或者洗澡的。

確實我沒和露媞一起進過這種正規的桑拿店。

「那就是初次體驗啊。這裡和皇宮的那種豪華的桑不一樣，是在暖爐裡放入加熱的石頭這種簡單的發熱方法，而且店裡還有其他客人會有點吵，不過我覺得這也不錯」
「那我也想試一試」
「好，那就走吧」
「嗯」

我和露媞進入澤弗的店內，店裡有好幾組客人正在一臉美味的享用著洗完桑拿之後的啤酒。早上的酒看來讓他們很愉悅。

「歡迎光臨。這不是雷德嗎」

澤弗露齒一笑，歡迎著我們。打工的青年正在忙碌地陳列著酒之類的物品，看上去並沒有功夫來招待我們。

「生意真是紅火啊」
「這都多虧了你啊。這邊的小姐臉挺生啊」
「她是我妹妹」
「啊？我可沒聽說你有妹妹啊」

澤弗盯著露媞看了起來。啊，這下可能不妙。

「⋯⋯呃」

他那滿是皺紋的臉上流下了冷汗。放在前台上的手不斷顫抖，發出著咔啦咔啦的聲響。

「澤弗，這是⋯⋯」

我慌忙思考起藉口來。
然而，澤弗閉上眼睛移開視線之後，用力做了個深呼吸。

「呼，抱歉。已經沒事了」
「啊，不是，是我們不好，我的妹妹她⋯⋯」
「顧客的加護還有來歷，我是不會過問的。小姑娘，抱歉讓你有了不愉快的經歷」

低頭道歉的澤弗讓露媞露出了很意外的表情。對她來說，應該是第一次見到這種反應吧。尊敬，恐怖，憎惡⋯⋯不管是什麼樣的感情，一般來說看到『勇者』的人都不可能會無視『勇者』

像澤弗這樣會和她保持距離做出對應的實屬罕見。
但這就是佐爾坦的作風。不會過問他人的過去。

「那，我們２人份的」

我把２枚４分之１佩里爾銀幣的費用交給了澤弗。
相對的從他那裡拿過衣櫃的鑰匙和毛巾後，我們離開了前台。

「在進去之前先給你說明一下吧」
「哥哥不和我一起去嗎？」
「嗯，男女房間是分開的」
「這樣啊」

露媞有些失望的樣子。

「不過，洗完桑拿之後這裡有飲料喝，還有小吃快餐之類的東西。這個時間雖然可能會有點不上不下，不過到時候一起吃點什麼吧」
「嗯」

接著，我把進入公共桑拿的方法告訴了她。露媞帶著以她來說很認真的表情聽完之後點點頭。

「大概就是這樣。那我就，我想想，今天時間就短一些洗個２０分鐘就出來，露媞呢？」
「我也２０分鐘」
「明白。你也不用在意時間可以慢慢來。就算想多洗一會兒我也會等你的」
「不要緊」
「是嗎，那好吧」

接著，我和露媞暫且分開，分別朝更衣室走去。

＊  ＊  ＊

之前是一直在和岡茲還有斯特姆桑達較勁，不過桑拿這東西可不是能強行長時間待在裡面的。

走出桑拿的我，用水罐裡面的水衝走了身上的汗。冰冷的水澆在火熱的身體上十分舒服。若是大貴族的桑拿，還設置的有用來冰水的冷卻爐，能夠享受到宛如隆冬的湖水一般的透心涼，當然佐爾坦並沒有那種魔法裝置。
不過就算沒有那種東西，只用普通的水也足以讓洗完桑拿後大汗淋漓的身體清爽一下。

「就洗到這兒吧」

雖然離說好的２０分鐘還有點早，我還是換好衣服回到了大廳。
之後又過了幾分鐘，露媞也在剛好２０分鐘的時候回來了。

「這邊」

我叫了她一聲，隨後露媞坐在了我旁邊的椅子上。

「想喝點什麼？」
「喝哥哥喜歡的就好」
「嗯，我想想⋯⋯那就喝水果牛奶吧」

向澤弗點單之後，過了一會兒倒在木杯裡的水果牛奶便被端了過來。
這是一種兼具著甘甜與酸味的美味飲品。
看向一旁，露媞也在大口地喝著牛奶。

「『噗哈』」

我們同時喝完，又同時放下了杯子。

「好久沒有像這樣和哥哥待在一起了」

露媞嘴角上揚著說道。

「剛開始旅行的時候，一直都和哥哥在一起。那時我還很弱，哥哥總是在幫我。還教會了我好多東西」
「剛出發的時候嗎。畢竟那時候你的加護等級很低呢」
「那個時候的旅途十分新鮮。而且還有哥哥陪著我」
「真叫人懷念」

故鄉的村子遭到魔王軍襲擊，我和露媞一起壓制了哥布林盤踞的廢棄礦井，讓村子裡的人還有周圍村子裡的人躲到那裡去避難。
之後我們襲擊了魔王軍的營地，解決武器商人的麻煩之後讓他為我們提供武器倉庫，然後籌備武器之後和武裝的村民們一起搶回了被佔據的村子。

在向整個大陸發起侵略的魔王軍看來，那只不是一處不值一提的戰場。敵人都是獸人的掠奪部隊還有被他們拉攏和怪物和盜賊，而惡魔也只有寥寥的幾隻下級軍官。無論是魔王軍還是阿瓦隆大陸軍，都沒有在那場戰鬥中投入主要戰力。

但即便如此，大家也都十分認真的對待著那場戰鬥。因為他們要守護的是自己的村子，還有住在那裡的家人。
這場與世界的命運相比要更加切身，更加庸俗的戰鬥，便是我們啟程開端的冒険。
對露媞來說，她是第一次離開村子，眼前所見所感的事物全都很新鮮。第一次看到獨角獸的露媞撫摸著獨角獸白色毛髮的模樣，顯得十分安詳。

然而⋯⋯伴隨著『勇者』加護等級的提升她也逐漸失去了這些。

「啊」

一道女聲打算了我的思考。
一名矮人少女看到露媞瞪大了眼睛。

「你是剛才甩水的那個人！」
「甩水的人？」

聽了她的話我歪了歪頭，接著露媞肩膀抖了一下。
少女露出了像是與英雄再會一般的笑容。

「她很厲害哦！從桑拿出來之後唰地澆了一身水，然後甩動了一下身體水就全部被甩掉了」

少女像是興奮的孩子一樣大聲這麼說完，好像是和露媞一起洗了桑拿的女性們也都紛紛看向露媞齊聲說道「真厲害」，你一言我一語了起來。

原來如此，露媞拿著的毛巾只有一條是濕的。
一開始我還以為她是沒帶毛巾進的桑拿，看來她好像根本用不著毛巾，只是抖動身體便把身上的水給甩乾了。這是她依靠超人般的身體能力所使用的技巧。

露媞有些不知所措地低下了頭⋯⋯嗯。

「很厲害吧？她可是我引以為傲的妹妹」
「她是大哥哥的妹妹嗎！？好厲害！！」

我的話讓少女高興地跳了起來。

「哎，她是雷德的妹妹啊⋯⋯」

周圍的人似乎也因為她的身份從不明人士變成了我這個藥店老板的妹妹而安心了下來。

「我還是第一次見到那麼厲害的技術呢」
「是啊，和雷德不同她看上去很強呢」
「她剛來佐爾坦，現在正在帶她觀光」
「哎呀是這樣啊」

接著她們的話題又轉移到了自己養的狗身上，和我們隨便打了個招呼便離開了。離開桑拿店之後，露媞顯得有些沮喪。

「對不起」
「幹嘛突然道歉」
「我沒打算那麼引人注目的」

露媞應該只是想有效率的晾乾自己的身體而已吧。她直到現在似乎都還沒有理解為什麼會那麼引人注目，只是覺得給隱姓埋名經營著藥店的我添了麻煩而感到沮喪。
我溫柔地撫摸著露媞的藍色秀髮。

「露媞。你在這裡不用顧慮那麼多」
「可是」
「確實我是隱瞞了自己的身份，但並沒有人會問過我們的事不是嗎？」
「嗯」
「就算你會顯得有些引人注目，在這個佐爾坦也不會有事。所以你不用顧慮那麼多隨心所欲的行動就好」

露媞那雙紅色的眼睛直直地看著我。宛如紅寶石色的湖畔一般的眼瞳出現了些許動搖。

「真的不要緊嗎？」
「啊，能向大家介紹我引以為傲的妹妹高興還來不及呢」

這是我的真心話。現在我也巴不得趕緊把露媞介紹給所有我在佐爾坦的熟人認識。但因為我現在也還不了解露媞那邊的情況不知道該怎麼樣去介紹她，所以現在選擇暫時擱置。

「我是引以為傲的妹妹嗎」

露媞小聲說道。雖然她的聲音小道就像是在喃喃自語一樣，但露媞的聲音我是絶不會聽漏的。

「你可愛善良又坦率。是我引以為傲的妹妹」

我直白地斷言道。
露媞以只有我能看出來的程度臉紅了起來。她在害羞。
這也顯得很可愛。

「好，那就繼續帶你觀光吧。接下來咱們去集市逛逛」
「嗯」

露媞朝我伸手。這種感覺，真讓人懷念啊。我握住了她的手。
我們２人手牽著手，走在佐爾坦平民區的大街上。

＊  ＊  ＊

哥哥的體溫從牽著的手傳達了過來。

現在拉著我的並不是什麼『勇者』，而是哥哥的手臂。
這讓我無比地開心。

「哥哥」
「嗯，怎麼啦？」
「沒事」

我那無所事事地呼喚讓哥哥微微一笑。這張笑容，比我得到的任何財寶都要寶貴。
太幸福了⋯⋯⋯

如果讓我實現１個願望的話，我希望今天能夠永遠持續下去。
但是把『勇者』這個加護推給我的正是神明⋯⋯那我到底該向誰許願才好？
這個問題讓我摸不著頭腦，我只得把這份願望悄悄地埋藏在了心底。

＊  ＊  ＊

夜晚。

我們４個人一起吃著晚飯，露媞雖然面無表情，但全身都散發著開心的氛圍。

「說起來，家裡有浴室啊。會旅館之前要不要先洗個澡？」
「洗澡，嗯我要洗」

在外出門旅行，能夠洗澡的機會並不多。
由於清潔感很重要，所以雖然我們會仔細地清洗擦拭身體，但一般來說都是用的水桶和毛巾。

在老家的村子裡，也是把古舊的大鐘給翻過來充當浴池的。
不過雖說是大鐘但那個大小也只能容納１個小孩子。
大人只會用水清洗身體，並不會去洗澡。

我的老家那塊地方信奉經常勤洗澡就不會生病的說法，所以為了能讓容易生病的小孩子洗澡村裡的鑄器店將因為古舊而被處理掉的教會的大鐘修好之後捐贈了出來。
因此我和露媞在兒時也能在３天裡洗１次澡。

「以前還一起洗過澡呢」

好像是想起了一樣的事，露媞懷念地說道。

那個浴池是直接在鐘下點火來加熱水的。
當然這樣一來底部會非常燙，所以會放上木製的踏板，下水的時候要踩在踏板上沉下去還得小心不要碰到底部。

洗澡的時候會有１個大人在旁邊看著，年齡較小的孩子也會有父母幫忙洗，不過我們兩個都很懂事能幹，因此在露媞２歳左右之後開始，便由我抱著她洗澡。
露媞用小手緊緊抱住我，和我一起下水之後，被父母評價為「這孩子不會哭不會鬧也不會笑」的她就會露出陶醉的表情。

這顯得非常可愛，所以直到迎來物理層次的極限之前我們都一直泡在水裡。露媞也不討厭洗澡⋯⋯我是這麼相信的。

「今天不一起洗嗎？」

太好了，看來她並不討厭。

「不，再怎麼說這個年齡也沒辦法一起洗了吧」
「這樣啊」

露媞看上去似乎真的很遺憾。嗯──，兄妹一起洗應該也沒什麼⋯⋯果然還是不行。

「那我想和莉特一起洗」
「誒？」

悠閑地坐在椅子上聆聽著我們聊天的莉特發出了驚訝的聲音。

「不行嗎？」
「⋯⋯這個嘛，嗯，可以哦。我也想和露媞慢慢地聊一聊呢」

莉特微笑著說道。露媞也輕輕一笑點點頭。
確實露媞和莉特至今為止都還沒怎麼聊過。
莉特曾在競技場裡被露媞打敗過一次，所以可能還是在怕她。
露媞本來話就不多，所以如果不主動向她搭話她基本上是不怎麼說話的。

這或許的確是個好機會。

「這樣啊，那我去放水吧」
「誒？」

接著發聲的事提瑟。她匆忙地揮舞雙手之後，

「我也可以一起洗嗎？」

這麼說道。
３個人的話感覺會有點擠，那就給附設的１人用小型浴缸裡也放上水好了。

「那我去準備，你們隨意」

我這麼說完打算起身的時候，提瑟不知為何閉上眼睛露出了做好覺悟的表情。

＊  ＊  ＊

海上。快速帆船西爾芙。一等客房。

之前在佐爾坦引起圍繞著惡魔加護所展開的所有一系列騷動，然後在和雷德的戰鬥中被砍斷右手而落敗的阿爾貝爾，正有氣無力地躺在床上。
他的腦袋正陣陣刺痛，令人不快的倦怠感仿彿壓的身子喘不過來氣。
然而即便如此，阿爾貝爾也不覺得現在的情況很糟糕。他甚至可以說心中還有一種滿足感。

『賢者』亞雷斯正在他的床邊。

「呵，呵呵，雖然不知道你想要去完成什麼樣的使命，但少了我賢者亞雷斯可是無法討伐魔王的啊露媞！」

亞雷斯雖然連續幾日施展了高強度魔法消耗巨大，但他那充血的雙目依舊炯炯有神，高舉著雙手如此大喊。
提奧朵拉正在給鐵青著臉躺在床上的阿爾貝爾施加治療魔法。充滿生命力的光輝讓阿爾貝爾眯起了眼睛。

佐爾坦並沒有能夠使用如此強大的治療魔法的人。過去和她組隊的『僧侶』莉亞自不必說，就連身為佐爾坦聖方教會最強的西恩司教也遠不及她。
然而即便施加了如此強大的魔法，也無法完全治好每天都在不斷流血的阿爾貝爾。

在露媞和提瑟乘著飛艇離去後，亞雷斯，提奧朵拉，以及被契約惡魔帶過來而茫然自失的阿爾貝爾３人留在原地露營了數日，無所事事地消磨著時間。
阿爾貝爾得知他們是『勇者』團隊，因來到了夢寐以求的地方而產生興奮，但更因為現在的自己是以骯髒的罪犯的身份來到了這裡而非『The・Champion』感到丟臉，絶望。

但是⋯⋯現在卻不同了。

亞雷斯的腳邊的地板上正四濺著鮮血，那是從阿爾貝爾的手臂上抽出來的血。
由於在用魔法堵住傷口之後又繼續增添了新傷，他的手臂上滿是歪曲的傷痕。
亞雷斯集中注意力後，鮮血蠕動起來形成了某種圖案一樣的東西，指向了一個方向。

「果然沒錯！露媞在世界盡頭之壁的方向！」

亞雷斯的喊聲響徹著客艙。『勇者』這個詞，讓阿爾貝爾虛弱的心臟用力跳動了起來。然而，與興奮的亞雷斯和阿爾貝爾相反，提奧朵拉的表情很冷淡。

「如果她去了世界盡頭之壁就麻煩了。想走海路去世界盡頭之壁的話，就算繞路也會變成一趟沒有補給的航行。中途必須要借用大型卡瑞克帆船或者軍用蓋倫帆船才行。沒有飛艇的我們可是追不上去的」

亞雷斯似乎無意回答她，只是看著四濺的阿爾貝爾的血在笑。

「這些血裡還殘留著契約惡魔的契約之力！就是想要到達勇者身邊的契約！這些血擁有指引勇者露媞所在的方向的力量！只要引發這個奇跡！我就還能追上勇者！」
「真是搞不懂你」

提奧朵拉用冷淡的視線看著如此大叫的亞雷斯喃喃道。
亞雷斯轉過頭來，瞪向提奧朵拉。他的雙目充滿殺氣，讓身為Ｂ級冒険者的阿爾貝爾也不禁不寒而慄。

「你說什麼？」
「勇者閣下是自己拋棄我們的。你再追上去又有什麼意義？」
「為了打倒魔王我賢者亞雷斯的力量可是不可或缺的！我只是為了拯救世界在拼盡自己的全力而已。反倒是你，又為什麼會在這裡？既然追上去毫無意義那麼你逃走不就好了嗎」
「讓你自己走的話阿爾貝爾可是會被你害死的」

亞雷斯表情扭曲，大步走向提奧朵拉揪住了她的前襟。

「我也會用治療魔法！而且和你同級甚至在你之上！可別忘了只是因為你說想給他治療我才讓你做的！」
「你錯了亞雷斯」

提奧朵拉露出了憐憫一般的表情。
這似乎讓亞雷斯更加煩躁。

「想要治人只靠技能是不夠的。如果治療者不能陪在傷患身邊，理解傷患的痛苦並治癒他，是無法去治療他人的」
「哈！無聊透頂！完全不明所以！你以為這種曖昧的理論騙得了我嗎！？」

現在的亞雷斯不管對他說什麼都聽不進去吧。如此判斷的提奧朵拉無力地搖了搖頭，靜靜地解開了亞雷斯抓住自己前襟的手。

「若不是人命關天，我就讓你吃點苦頭長長記性了⋯⋯總之阿爾貝爾的治療和健康管理就交給我。在你找到勇者閣下之前，我一定會讓他活下去」
「你可別以為這種事就能賣我人情了」
「我可沒在賣你人情。我只是作為一名微不足道的聖職者，以及拯救世界的勇者閣下的同伴，在盡自己應盡的義務。既不是受人命令，也不是為了賣別人人情或者得到他人感謝在戰鬥。因為自己想要拯救世界才去戰鬥的。至少我是如此」

亞雷斯用猙獰的表情瞪了一眼提奧朵拉之後，像是不想再和她待在一起一樣氣洶洶地離開了房間。
提奧朵拉看著地板上四濺的鮮血，為了像往常一樣善後打算拿著水桶去打水。

「需要我幫忙嗎⋯⋯？」

阿爾貝爾的話讓提奧朵拉露出有些驚訝的表情，回到了他旁邊。

「不用在意，你好好休息就行」
「⋯⋯我有幫上大家的忙嗎？」

阿爾貝爾用無力地，但也不帶惡意地純粹的目光看向提奧朵拉。

「這我就不清楚了。但是多虧有你在我們才得以接近勇者閣下。不管今後會發生什麼，我們都會依靠自己的意志去選擇結果，而不是交給他人。在這一點上阿爾貝爾，你是不可或缺的。謝謝你」
「是嗎⋯⋯」

阿爾貝爾的嘴角浮現出祥和的笑容。

接下來他們要前往世界盡頭之壁。而且走的還是南邊的海路⋯⋯離佐爾坦很近。
失去的右手突然一陣刺痛。

「下一處靠岸的地方是交易都市拉克。在那裡買只義手吧」

看著阿爾貝爾的模樣，提奧朵拉說道。

「拉克是座靠與群島諸国的貿易而繁榮起來的城鎮，應該會有『錬金術師』製作的義手。就算握不了劍，但有一只能動的右手在至少應該也能減輕一些痛楚」
「但是那樣豈不是會耽誤行程⋯⋯」
「不用在意。若沒有你的話我們根本追踪不到勇者閣下的行踪。這點時間還是耗得起的」

言畢提奧朵拉露出了微笑。
阿爾貝爾看向自己那從手腕部分開始消失不見的右手。

「砍斷這只手的男人，是個不可思議的傢伙」

他回想起那個佩戴著銅劍的Ｄ級冒険者的模樣。
那傢伙很強，強到自己甚至不知道和他之間到底存在多大的差距。

「明明有那麼強大的力量，為什麼不去成為英雄」

阿爾貝爾並沒有期望能夠得到回答。他只是喃喃低語一般在捫心自問。
然而提奧朵拉卻神色認真地看向阿爾貝爾。

「人必須選擇符合『加護』的生存方式才行」
「這是聖方教會的教誨吧」

『加護』是由至高神迪米斯所賜予的。而擁有『加護』之力的人，會被要求履行與加護相吻合的職責。
身為『十字騎士』，亦是聖地拉斯特沃爾大聖城的聖職者的提奧朵拉既然這麼回答了，那應該就是正解吧。阿爾貝爾如此理解到。
然而提奧朵拉卻搖了搖頭。

「但是『加護』並不是人」
「誒？」
「人是有思想的。自己想要踏上的人生，夢想，未來⋯⋯就因為擁有能夠成為英雄的我『加護』，難道就必須作為英雄而活嗎？就不能選擇其他道路嗎？」
「但那不是神所期望的嗎」
「那麼為什麼神要賦予人思想。如果說完成『加護』的職責便是一切，那麼我們豈不是根本不需要思想嗎？」
「這個⋯⋯我也不清楚」

阿爾貝爾並不是神學者，也不是什麼熱情的信徒。不可能和身為聖職者的提奧朵拉進行神學問答。

「抱歉。其實我也在尋找答案」
「提奧朵拉大人也在迷茫嗎？」
「勇者閣下拋下了我們。我還沒有開悟到遇到這種事還能毫不迷茫」

提奧朵拉苦笑起來。

「砍斷你的手的男人，叫什麼名字？」
「他叫雷德」
「雷德嗎⋯⋯真想會會他」

提奧朵拉如此低語過後，為了去拿清洗地板上血漬的水桶和刷子離開了客艙。
阿爾貝爾目送著她離開之後閉上了眼睛。
果然體力似乎還是吃不消，他很快便陷入了沉睡。

＊  ＊  ＊

滴答。１滴水從天花板落下的聲音響起。

盯著漸漸擴散開來的漣漪，我明明是泡在暖乎乎的熱水裡卻感覺到一股寒氣。

我叫提瑟。
擁有刺客的加護，是勇者露媞大人的朋友。

現在，我正在泡澡。實不相瞞我十分喜歡泡澡，在我們這些殺手之間外號「評論家提瑟」

我趁著工作之餘所記錄了各種都市的公共澡堂，溫泉聖地，還有驛站服務區的澡堂設施之類的的評價手冊，現在可是殺手公會的成員指定遠征計劃時所必備的寶物。散漫的暗殺工作是需要滋潤調理的。
不過需要脫光的澡堂對於暗殺工作來說也是絶好的時機，因此像是武器的保管位置，還有逃跑路線等等，那本萬能的手冊也能用於工作之中。

而在喜歡泡澡到甚至撰寫過手冊的我看來，這個浴室⋯⋯評分很高。
首先，有２個家庭用的浴缸就是一個很高的得分點。
現在我所在的壺型浴缸，是一個可以重視自己個人空間的產物。

社會是一個需要交際能力的世界。即便是殺手也不例外。不如說，每次都需要扮演不同的自己潛入都市內的殺手，正是一種身處交際能力的重壓之下的職業。
無論何時我們都不能隨意開口。需要時常注意自己想說些什麼，以及說的話會帶來產生什麼樣的影響，控制好自己的交際能力才行。這是一件非常費心勞神的事。
在殺手裡面，也有許多殺人的本領雖然是一流，但就因為交際能力不過關而永遠無法出人頭地的前輩。

我是在師傅的嚴格指導下學習的偽裝技術，因此不管是什麼樣的角色我都能勝任。但這並不代表我喜歡偽裝自己。
也就是說，我現在泡在自己專用的浴缸裡，能夠在這個壺型的空間裡體驗到名為提瑟・加蘭德的自己被填滿的幸福感。

水是通過管道加熱的這點也很棒。
如果洗澡水是有人在外部加熱的話，無論如何都會去在意起那個人來。
但是這個浴缸只需要稍微探出來一點身子調整一下閥門就能夠改變溫度。

「就給４星吧。可惜的是浴缸太深如果我坐下去的話水會浸到嘴巴」

我的話變成了噗嚕噗嚕的聲音，沒有任何人聽到。
我的個子很矮。雖然對於使用從背後給予要害處銳利一擊的戰鬥方式而非用強大的力量進行壓制的我來說嬌小的身體更加有利，不過這讓我在生活在也會遇到很多不便。

嚇嚇先生捉到了被浴室的濕氣吸引飛來的虫子正在用餐。
看著它用前足緊緊抱住獵物一臉美味的進食的模樣，讓我感到心裡暖暖的。唉，還是不要逃避現實了，直視眼下的狀況吧。

其實也並不是說出現什麼問題啦。
只是勇者露媞大人和莉特小姐正泡在１個浴缸裡而已。

（插圖００４）

在我至今地近距離觀察下發現⋯⋯露媞大人喜歡她的哥哥基甸先生。而且還是一心迷戀。
但是基甸先生卻和莉特小姐兩情相悅。一看就能看出來他們很親熱。
對基甸先生來說露媞大人只不過是他最心愛的妹妹。和對於莉特小姐的喜歡應該是不一樣的。

「泡起來很舒服吧？」
「嗯」

面對面的這２個人根本聊不起來。

從剛才開始露媞大人就一直緊盯著莉特小姐，而對於莉特小姐的話她只會做出簡單的回應。真虧莉特小姐能夠忍耐得了這種狀況。
就算露媞大人自己沒有惡意，沒有超常的膽量也是不可能能夠直面『勇者』的。
即便是和露媞大人交上朋友的我，待在她旁邊也遠比直面她輕鬆得多。

而現在我無法斷言露媞大人對於莉特小姐沒有任何的惡意。
我正是為了以防出現什麼意外，才會一起進來洗的。

「莉特」

露媞大人終於主動搭話了！
我膽顫心驚地泡在水裡，保持著隨時都可以衝出去的狀態。

這只不過是為了謹慎起見！

「怎麼啦？」
「莉特已經和哥哥一起洗過澡了嗎？」

上來就一針見血！好可怕！

「對啊」

毫不留情地給予了反擊！好可怕！
２人之間的氣氛並不險惡。

雖然並不險惡，但身為殺手見識過無數次人在渴望他人死去的場面的我很清楚，戀情是會成為殺人動機的。

「我也洗過。雖然是很早以前」
「雷德⋯⋯基甸小時候是怎麼樣的？」
「和現在沒什麼區別」
「意思是他沒有成長嗎？」
「不對。我是說哥哥永遠都是那麼的帥氣」

露媞大人微微伏下了眼簾。
仔細一看，她的臉紅了起來。

「過去，我很弱」
「是嗎？看著現在的你我不敢相信哎」
「真的。雖然都在說我的第一場戰鬥的對手是襲擊了村子的獸人輕騎兵，但不是的。我的初戰，是在去附近的山裡尋找走失的孩子時發生的」
「走失的孩子？」
「那時我只有５歳。我也還是個小孩子。但是因為我是勇者所以不能坐視不理」
「加護的衝動呢⋯⋯」

莉特小姐表情認真地低語著。

加護的衝動是這個世界上所有人的共同煩惱。
是過上加護所希冀的人生，還是反抗加護度過自己的人生。
很多人都選擇了過上加護所希冀的人生。因為要不斷反抗衝動是一件很痛苦的事，而且加護還會贈予對那個人生有幫助的技能。

但那並不見得是本人所希望度過的人生。
在我思考著這些的時候，露媞大人正一反常態變得能說會道，講述起勇者最初的冒険的故事。

＊  ＊  ＊

在初春的某天，一名和我沒什麼關係的女孩，在從冬眠中甦醒的飢餓動物們為了尋求餌食而四處游蕩的山中走丟了。

哥哥那天出門不在家。我沒有其他可以依靠的人。
父親和母親也沒有強到能夠進入這個季節的大山。
山上還殘留著些許皚皚白雪。河流那邊會傳來咣咣的聲響大概是冰雪融化所發出的聲音吧。

對於５歳的我來說，即便是非怪物的一般動物也是致命的對手。
我的手裡就只有一把靠不住的匕首。

暮色正在降臨，我一邊呼喚著那個女孩的名字，一邊小心不被大山之中的威脅包圍在不斷移動。
察覺到氣息之後回過頭去，一只巨狼正在用打量般的視線看著我。

但不知道是不是沒胃口吃我這種不起眼的小獵物，它不感興趣地移開視線後消失在了黑暗的森林中。
若換成是普通的小孩子即便是鐵打的勇氣也會被溶化，然後慘叫著逃走吧，這是很正常的。但是我卻感覺不到恐懼。

我只是認為威脅離去，然後繼續著這趟危險的冒険之旅。
在周遭已經完全暗下來的時候，我終於發現了那個女孩。

找不到回去的路的女孩，好像發現了一處很溫暖的洞穴，然後躲在裡面哭泣。附近的樹上殘留著巨大的獸爪痕跡，洞穴裡也在散發著強烈的野獸臭味。
如果擁有知覺技能的話，應該就能發現隱藏在洞穴深處的巨大存在了吧。

這個孩子已經被魔獸梟熊當做獵物了。
梟熊是這座山的食物鏈的頂點。就連剛才那匹狼應該也無法逃出梟熊的魔爪。本應立馬被吃掉的那個女孩之所以還活著，應該是因為梟熊已經在其他地方進過食了吧。
現在不殺她，是因為它知道人類的孩子很容易就能殺死。它只是為了能盡可能地在新鮮的狀態下吃掉她才將她放置不管。

雖然我擁有『勇者』的加護，但當時的加護等級只有１級，身體也還是個小孩子。對手是據說加護等級１５以下根本不可能打倒的梟熊，實力差距顯而易見。但是我卻不能坐視不理。這大概就是『勇者』加護的缺陷所在吧。

加護絲毫不畏懼死的恐怖，比起生存選擇將作為一名『勇者』放在了第一位。
而且，我壓根就用不著去考慮這些事情。

「露媞！！」

因為看到我的那名女孩，大聲喊著我的名字哭著跑向我，將有新的飼料出現的事告訴了洞穴中的梟熊。

「咕噢噢噢噢噢噢噢！！！！」

梟熊發出咆哮衝了出來。
我將匕首拔出反手握持。勝率微乎其微。但是不贏的話就只有死路一條⋯⋯機會只有一次。衝過來的梟熊朝我揮下了爪子。它的速度實在太快，根本來不及躲閃。

所以，我用左手抵住自己的胸口，等待著那個瞬間。
接著我的身體便被梟熊撕裂。

「治癒之手」

但是本應被撕裂的我卻毫髮無傷。
我全力使用「治癒之手」，在被撕裂的瞬間治療了自己的身體。
被攻擊到的對手竟毫髮無損，這件事大概也出乎了梟熊的意料吧。
我趁虛而入，用反手持的匕首扎向了梟熊的左眼。梟熊因痛苦發出了咆哮。

（太淺了⋯⋯）

雖然全力刺出了匕首，但我的匕首只貫穿了它的眼球。
傷口很深，或許遲早這道傷會要了它的命。
但若想要對它造成無法行動的致命傷，刀刃就需要刺入它的大腦才行。

咚地一聲，我被梟熊揮舞的手臂打中飛到了空中。
因為打到我的並不是爪子，所以並沒有當場死亡。但也就僅此而已。

我的身體在地面不斷滾動之後，終於停了下來。雖然我拚命地想要舉起匕首，但手臂卻在無力地搖動⋯⋯骨頭斷掉了。
自己已經盡了全力。這是無可奈何的。

不知是不是加護也承認了這點，它並沒有讓我站起來去送死。它似乎大發慈悲，允許我在最後躺著死去。

反正就算活下去，也只會像這樣為了毫無瓜葛的人留下痛苦的回憶。
反正就算活下去，也只會留下痛苦的回憶卻還要在背後被人說我讓人毛骨悚然。
反正就算活下去，也只會被嘲笑我讓人毛骨悚然的人在需要的時候尋求我的幫助。

已經夠了。我已經活了５年了。
如果從懂事的時候開始算的話時間會更短。

但對於那時還未擁有絶望耐性的我來說，這段時間足以讓我對人生感到絶望。

然而⋯⋯有１個人。只有１個人，從未向我尋求過幫助。
他總是會在我需要幫助的時候來幫我。
他會說我是他可愛的妹妹而疼愛我。

即便拋棄其他的一切我也在所不惜。不管是父母，還是故鄉，乃至世界。
可是，唯獨只有再也見不到哥哥這件事⋯⋯我不要。

想到這裡，話語便搶在思考之前自然地脫口而出。

「哥哥救我！！」

白刃如雷光一般游走。
從眼睛被捅傷而造成死角的梟熊左側刺來的劍，貫穿厚重的肌肉鎧甲破壊了心臟，一刀便了結了這只７００公斤重的巨軀。

「露媞！要不要緊！竟然受這麼重的傷！！」

那個人沒有為打倒梟熊感到驕傲，甚至一眼都沒有瞧過那份豐功偉績，只是看著受傷的我⋯⋯流下了了淚水。

「抱歉，我來晚了，真的很抱歉⋯⋯」
「我不要緊，因為哥哥救了我」

但是我根本不在意那些痛楚。

因為眼前的這個人，在我痛苦的時候總是會陪伴在我的身邊。也會為了我而流淚。
這讓我無比地開心，勝過痛苦以及其他的一切。

＊  ＊  ＊

講完之後的露媞大人，一直在盯著天花板。
她的臉會發紅大概並不是因為泡昏頭的緣故。

「露媞也是這樣啊」

莉特小姐也看向了天花板。似乎是在回憶什麼。

「我的老師，還有相信我肯助我一臂之力的近衛兵團和冒険者們都被席桑丹殺死，讓我陷入了絶望。明明還向你們誇下海口說自己的国家要由我們自己來保護。那時的我，覺得自己根本不該被生下來。一切都是因為自己」
「這樣啊」
「就在那時雷德拯救了我。他為了我搶在你們到達之前便衝到席桑丹面前和他打了起來。鼓勵我與其在那裡後悔不如去報仇雪恨」

這應該是在羅加維亞公国戰役中發生的事吧。莉特小姐閉上眼睛似乎在回憶當時的情景。

「露媞應該也看到了，我在迷惑之森裡的曾無數次快要陷入絶望。覺得自己一直在同樣的地方來回打轉，在森林裡持續走了１個星期⋯⋯想著羅加維亞是不是已經敗北，大家都被殺掉了」

明明話題很陰暗，莉特小姐的表情卻很開朗。
那可能是段辛酸的記憶，不過同時也是和基甸先生邂逅的回憶。

「但是有雷德在我身邊。他和我並肩作戰。讓我去拯救羅加維亞。在連陽光都沒有的昏暗的迷惑之森裡，有雷德陪在身邊讓我無比開心。那種心情我還是第一次出現」

莉特小姐抱住膝蓋擋住嘴角笑了起來。

哎呀，原來如此，基甸先生是這樣一個人啊。
不斷遭遇那種體驗的話，也怪不得露媞大人和莉特小姐都會喜歡上基甸先生。露媞大人用雙手舀起一灘水。
從手掌中漏下的熱水，發出嘩啦啦的聲音。

「我現在洗澡，並不像以前那樣覺得那麼舒服」
「誒？」
「會覺得洗澡舒服，是因為泡澡之後會溫暖身體，讓血液循環，治癒疲憊的肌肉」

露媞大人又舀了一灘水。浴缸裡響起了嘩啦嘩啦的聲音。

「我的身體擁有一切耐性。不管遇到什麼樣的極寒酷暑，體溫都不會有變化。洗澡水的溫度也有一樣，對我來說這只不過是一灘熱水」

嘩啦。

「既不會生病也不會感到疲勞。身體一直都保持著最良好的狀態」

嘩啦。

「也不用吃飯。我不會感到飢餓。也不用喝水。雖然能嘗出味道，但我的身體不需要營養」

嘩啦。

「我會覺得洗澡舒服，是因為我擁有覺得洗澡很舒服的回憶。只不過是通過回憶將感情再現出來而已」

嘩啦。

「小時候，我覺得哥哥沖的蜂蜜牛奶非常好喝。味道又甜又溫柔，不管有多少都能喝完。但是，今天的蜂蜜牛奶，明明味道比過去還棒，我卻覺得不如過去那麼好喝。可即便如此，我也擁有覺得哥哥的蜂蜜牛奶很好喝的回憶」

這樣啊⋯⋯這就是使問題復雜化的原因嗎。

露媞大人是人類最強。
我和亞雷斯大人，達南大人和提奧朵拉大人，以及基甸先生也是處以人類頂點的強者。
但是我們並不是最強。就算我們加在一起也贏不過勇者。露媞大人無法再次體驗到被他人幫助的感覺。

感情也不像過去那樣有很大的起伏。有害的感情基本上都被露媞大人的加護給剔除了。所以，露媞大人只能懷戀過去。

（⋯⋯露媞大人只剩下基甸先生了嗎）

莉特小姐也一臉驚訝的表情一言不發。
這⋯⋯就是勇者的加護嗎。人類的希望，被神所選中的英雄，世界最強的力量。

「在羅加維亞和莉特一起行動的時候，我很討厭你」
「畢竟那個時候的我給人一種不能把問題交給勇者之流來解決的感覺呢」

莉特小姐苦笑起來。

「不是的。我是很羨慕你。能夠自由地歡笑，自由地生氣，自由地哭泣⋯⋯自由地戀愛。我十分羨慕不斷和哥哥縮短距離的莉特，十分羨慕⋯⋯」

滴答一聲輕響。
那是小水珠從露媞大人的雙目落入水中的聲音。

「真的讓我好羨慕⋯⋯所以我討厭你。所以，雖然哥哥和亞雷斯說應該邀請你入隊，我卻沒有去找你」
「⋯⋯露媞」
「莉特，提瑟⋯⋯這就是我」

露媞大人露出了我和莉特小姐也能看得出來的，明確的笑容。

「這就是『勇者』露媞⋯⋯莉特，我不想成為什麼『勇者』，而是想成為你」

是我誤會了。我真的是太不中用了。只會去擔心多餘的事，卻完全沒有弄懂真正的問題所在。在場的不應該是我，而是基甸先生才對。

一定只有基甸先生，才能夠拯救露媞大人。
露媞大人的笑容，悲傷到讓人不忍直視。

一只小手拍了拍我的肩膀。回過頭去我發現是嚇嚇先生。

「誒，不對？」

嚇嚇先生舉起了雙手。
如果嚇嚇先生能夠出聲的話，它應該是在大叫吧。

（沒什麼中不中用的，接下來才剛要開始呢！！）

嚇嚇先生這麼喊道。對啊，嚇嚇先生說的沒錯。

被『勇者』所囚禁，連自己的思想都無法自由支配的公主殿下。
拯救公主殿下的英雄是基甸先生。
那麼我就是「引導」英雄的魔法師嗎？

忽然我感受到了一股視線。莉特小姐正在看向我。我們的視線相互交錯。
莉特小姐的視線中蘊含著堅定的意志，她輕輕點了點頭。雖然時間很短暫，但莉特小姐也是和勇者大人一同冒険過的同伴。引導英雄的魔法師是２人加１只。

故事的角色分配大概就是這樣了吧。被囚困的公主已經足夠苦惱了。
那麼接下來就該由我來引導英雄，然後由英雄去和抓住公主的惡龍戰鬥，救出公主。我並不清楚該如何才能拯救露媞大人。

但是，我跟嚇嚇先生和露媞大人是朋友。
所以即便看不到終點，接下來我們依舊要勇往直前！

掀開這個勇者大人獲得拯救，大家一起歡聲笑語的故事的第一頁！