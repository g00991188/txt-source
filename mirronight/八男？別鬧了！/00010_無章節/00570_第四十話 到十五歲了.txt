 到十五歲了

「累死了……」

「我也是…..」

「俺、幾乎沒有感到疲勞、來吧、向俺打出最強的一擊！」

我們把王都做為據點大約兩年半後、在數天前十五歲的我、今天也和露易絲一起跟阿姆斯特朗導師努力修行。

『到底、是哪裡連載的戰鬥漫畫啊』想這麼說已經兩年半了、不過、成果充分的表現出來了。

沒有成果出來、我精神層面會陷入悲慘的情況中。

我的仍然魔力持續上升著、另外也學到了大量的魔法。

其中最該關注的果然還是阿姆斯特朗導師所教的體能超強化、高速飛行、魔導機動鎧甲這三個。

魔力雖然像笨蛋一般消耗但那威力是有保證的、前幾天從阿姆斯特朗導師獲得合格了、已經足夠運用在實戰當中。

嘛、本人的話、平常都是使用其他的遠距離魔法完全用不上就是了。

或著說、不使用這個(魔導機動鎧甲)就無法與他戰鬥的敵人、完全不想遇到阿。

還有、和我一起學習的露易絲她成功將體能超強化、高速飛行、魔導機動鎧甲、冥想這四個魔法學起來了。

受到魔力上升的影響、現在也學會了使魔力僅僅在身體裡循環的狀態魔法。

此外、她也掌握了原創魔法『冥想』。

使用冥想能治療自己的傷口、這是連我和阿姆斯特朗導師都無法學習十分特殊的魔法。

很可惜不能夠治療別人的傷口、但是在戰鬥中能治療治療自己傷口也是十分寶貴的。

就同一個隊伍的成員來看、出現複數的負傷者時、自己能想辦法搞定、隊員會輕鬆許多

綜上所敘、今天是我們三人最後一次的戰鬥模擬訓練。

不、這樣說比較正確。

這兩年半、三人幾乎每天都在戰鬥模擬訓練——在王都的郊外進行戰鬥模擬訓練。

消耗龐大的魔力戰鬥到極限、使魔力量增加、其他的訓練等戰鬥模擬結束後再進行就好了。

唯一的誤算就是『由於會破壞軍隊的練兵場、所以希望你們去誰都不在的地方訓練』被軍隊的領導這麼說了。

這也是、在這個大陸尋找沒有任何人的地方完全沒問題。

　それも、この大陸にはいくらでも無人の土地があるのでまるで問題はなかったのだが。

如此這般我們缺乏練習的場地、主要問題是阿姆斯特朗導師異常的破壞力。

そんなわけで練習場所には事欠かなかった俺達であったが、問題なのは旗振り役であるアームストロング導師が異常にハイテンションであった事であろうか。

總之、阿姆斯特朗導師的魔法是超出常理外的。

對龍、用魔力將魔導杖的形狀改變成鎚子、穿上魔導鎧甲甚至能徒手毆打。

抓住它的尾巴扔出去、吸收魔力實體化的裝甲和武器防止(肌肉)撕裂傷。

當然可以教給別人、但那不是那麼容易模仿的。

強行教給其他魔法師、它(魔導鎧甲)反而會成為麻煩。

如果魔力只有一般的魔法師使用、魔力馬上就會見底。

因為那樣的理由、自己一個人持續鑽研著。

另一方面、魔力很多但不能夠使用魔法所以只能在老家修練魔闘流的露易絲、魔力也上升到連我都訝異程度。

她對那樣的事情欣喜若狂、在這兩年半我和露易絲每天都在嚴酷的訓練中渡過。

但是、那也在今天就結束了。

明天成員裡生日最慢到來的伊娜也十五歲了、終於要作為冒險者開始活動。

所以今天是和阿姆斯特朗導師進行最後一次的戰鬥訓練

還剩下幾分鐘、全力戰鬥腎上腺素全開的阿姆斯特朗導師姑且不論、我和露易絲受不了極速消耗魔力的疲勞感。

「最強的一擊啊…….」

「威爾、實際上、我已經在一擊就到極限了」

「那麼、殺吧」

「誒！」

「不、不抱持著那種心情不行吧」

在王都郊外的上空、我與露易絲和浮在上空的阿姆斯特朗導師對峙。

因為一起把擁有的技能持續擊出、在這之中魔力最低的露易斯差不多迎來極限了。

那麼、不抱持著殺了導師的覺悟把剩餘的魔力全擊出是不行的。

並不是我特別的怨恨導師。

很多部分都太超過的人、但並不是個壞人、現在也受著他照顧。

多虧了魔法格闘技的修行身體能力也上升許多、我想不會像以前的武藝大會一樣那麼難看、第一輪就輸掉。

因為也不打算參加第二次、所以也沒辦法確認就是了。

「兩年半來、從導師身上得到了許多指導、這裡應該全力以赴來回報他的恩情才對」

話說、這種程度全力應該也不能讓導師無法戰鬥。

因為他還從露易絲身上學習了高機動的體術以及技巧、比以前更加強悍。

魔力也因為和我進行器合增加了、變得越來越強大的敵人。

「這也是為了回報導師恩情！」

「(這是真心話？)」

「(兩年半來、我們是軍隊的新兵、道場的新人、在這裡趁亂打垮他也沒什麼吧！)」

強行施予恩惠、過於殘酷的野外實戰訓練等等的、果然還是有許多怨念存在。

「(說沒有半點怨恨肯定是騙人吧！笨蛋！笨蛋！)」

「(威爾、又不是小孩子了………)」

每天午餐都在野外採集什麼的、我還以為『這是哪兒的陸上自衛隊阿！』

「那麼、露易絲怎麼樣呢？」

「我確實也感到十分的辛苦」

再小時候就辛苦進行魔闘流修行的露易絲都說辛苦了、裡面是原現代人的我當然感到更加辛苦。

不要小看原現代人豆芽(脆弱)的精神。

「露易絲、比我和導師的魔力還少吧」

那是因為必須一邊節約魔力一邊對抗導師的力量。

「那方面、總會有方法的、對了、威爾如何呢？」

「疲倦感還可以想辦法應付、幾分鐘左右是沒問題的」

「最近越來越怪物化了」

「那是正面的稱讚、還有露易絲不也是嗎………」

「それは、正面の御仁にも言ってくれ。あとは、ルイーゼもか……」

實際上就露易絲這兩年半修行的魔力量、水平已經上升到近高級了。

靠著和我器合、魔力量達到界限。

雖然能使用的魔法很少、但現在大概也可以單獨把龍揍飛。

原本就持有大師級魔闘流的實力。

與通過使用大量魔力來提升攻擊力和防禦力的我跟阿姆斯特朗導師不同、更加迅速靈巧的戰鬥。

「威爾連發高集束魔力彈牽制、那之後我在打出全力的一擊」

「最可靠又確實的作戰啊」

發動魔導機動鎧甲的魔法、全身包覆輕薄鎧甲的露易絲向我提出作戰方案。

她的魔導機動鎧甲是三人中最輕薄防禦力最低的。

這也是因為要保有魔力的關係、她的身體能力和動態視力都很優秀、基本上是迴避敵人的攻擊、把魔導機動鎧甲作為最後的防禦手段。

這是因為魔力的多寡是我、阿姆斯特朗導師、露易絲順序排列。

格鬥能力高低的排列選擇當然是露易絲、阿姆斯特朗導師、我。

「高集束魔力彈、已經足夠派上用場了」

「瞭解」

我跟阿姆斯特朗導師近身戰鬥的經驗並不是很多、所以用遠距離高集束魔力彈攻擊來牽制他的動作。

以前、驅除古雷特古蘭多的時候、阿姆斯特朗導師放出了蛇型的風屬性高集束魔力炮。

我一直不懂為什麼要特意作成蛇的形狀、不過魔法會隨著本人的想像威力增強。

所以、那個蛇型的高集束魔力炮比較適合他吧。

我的情況是受前世的影響一般發射高集束魔力時、會變成砲彈型的高集束魔力彈。

數十發同時發射、一個個向阿姆斯特朗導師飛去。

我想這種魔法有著不輸給導師的實力在。

「喔喔！ 還是老樣子不容小看的攻擊呢！」

雖然這麼說、阿姆斯特朗導師用著雙手像是蒼蠅拍一般將那(砲彈)一個一個打掉了。

砲彈型的高集束魔力彈墜落在原野上、在這一帶的戰場留下了滿滿的隕石坑、但現在誰也不會提出抗議了。

任何事情都行、開墾這片土地。

當作在翻土、就沒有的任何問題。

「還是一樣、不知道極限在哪的魔力」

已經不知道發出幾百發了、但阿姆斯特朗導師還是一樣相當從容的用手彈開高集束魔力彈。

「(這個人、真的超過了四十歲嗎？)」

到現在還持續成長的魔力、與那成正比越來越向怪物進化的戰鬥能力。

最終、這個世界上存在著能殺了他的人嗎？

真不愧是他、魔力差不多到達危險地帶了、但在魔力上我還是略高一籌。

仔細看的話、能確認阿姆斯特朗導師的魔導機動鎧甲上逐漸出現裂痕。

最終、魔力的極限到來了。

然後露易絲終於移動了。

「喝！」

似乎沒有特殊的技能名

或是說、叫技能的名字之類的也只是浪費時間、我在這個世界裡還沒看過那種武術家。

露易絲、一瞬間突入了阿姆斯特朗導師動作的死角、從那裡聚集全身的魔力踢出。

遭受踢擊的阿姆斯特朗導師魔導機動鎧甲碎裂、他就那樣被擊落地面了。

在落下的地面上、大量的塵土和巨響與巨大的坑洞一起出現。

「可惜、魔力不夠了」

要是一般人會有是否還活著的疑慮、不過即使沒有魔導機動鎧甲還是有身體強化魔法在作用著、這點程度的衝擊阿姆斯特朗導師應該也不會怎樣。

他一邊抖落在長袍上的塵土和我們搭話了。

「真是的、二打一還是不太好」

「也是呢……」

雖說如此、但要打倒他還是需要兩人。

這是證明這人是多麼變態的證據。

話說、這裡因為攻擊的疲勞現在就快要倒下了。

今天確實在短時間裡使用魔力過量。

「合格了、不過少年跟俺魔力的鍛鍊還是不行、期待再次戰鬥的時候」

這兩年半、我和阿姆斯特朗導師拚命的增加魔力量、但仍然沒有到達界限。

畢竟我還只有十五歲、而不可思議的是一般魔法師從現在開始到二十歲之前才是魔力增加最迅速的高峰時期。

俺はまだ十五歳なので、むしろ普通の魔法使いはこれから二十歳前くらいまでが最も魔力量が増大のピークとなるので不思議はなかった。

然而阿姆斯特朗導師是還繼續增加著魔力的稀有例子。

也許那裡面的魔力暴走變成兇殘魔物的可能性也有。

當然、這不是個冷笑話

「俺會看著你們作為冒險者的成就」

其實看著我什麼的可以不用、總之能擺脫這些嚴酷的訓練就好、我和露易絲都露出了安心的表情。

但是、非常的困………

